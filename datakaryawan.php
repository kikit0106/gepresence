<?php 
session_start();
error_reporting(0);

require_once 'include/db_function.php';
$db= new db_function();

if (isset($_SESSION['admin'])) {

	$data=$db->getAllUser();
?>
<div class="col-xs-12 col-md-12">
              <div class="box box-danger">
                <div class="box-header">
                  <a id="tambah-kar" class="badge bg-red pull-left" data-toggle="modal" data-target="#modal-tambah" data-backdrop="static">Tambah Data</a>
                  	<div class="box-tools pull-right">
            		    <button title="Collapse" class="btn btn-box-tool" data-toggle="tooltip" data-widget="collapse"><i class="fa fa-minus"></i></button>
        			      </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="data-kar" class="table table-bordered table-striped table-hover">
                    <thead>
                      <tr>
                        <th style="width: 10px;">No</th>
                        <th>Nama</th>
                        <th style="width: 20px;">Shift</th>
                        <th style="width: 70px;">Mac address</th>
                        <th style="width: 170px;">Email</th>
                        <th style="width: 70px;">No. HP</th>
                        <th class="hide">Jatah Cuti</th>
                        <th style="width: 50px;">Pengaturan</th>
                      </tr>
                    </thead>
                    <tbody>
         <?php 
         $i=1;
         foreach ($data as $value) {
					 ?>
					           <tr>
                        <td><?php echo $i; ?></td>
                        <td class="td_nama" data-nama="<?php echo ucwords($value['nama']); ?>"><?php echo ucwords($value['nama']); ?>
                  <?php 
                  $admin=0;
                  if ($value['jabatan']!=null) {
                    echo "<small class='td_admin label label-success pull-right'>admin</small>";
                    $admin=1;
                  } ?>
                        </td>
                        <td class="td_shift"><?php echo $value['shift']; ?></td>
                        <td class="td_mac"><?php echo $value['mac_address']; ?></td>
                        <td class="td_email" data-admin="<?php echo $admin; ?>"><?php echo $value['email']; ?></td>
                        <td class="td_telepon"><?php echo $value['telepon']; ?></td>
                        <td class="td_jatah hide"><?php echo $value['jatah_cuti']; ?></td>
                        <td>
                        	<a class="badge bg-light-blue pull-left edit-kar" data-idkaryawan="<?php echo $value['id_karyawan']; ?>">Edit</a>
                        	<a class="badge pull-right hapus-kar" data-id="<?php echo $value['id_karyawan']; ?>">Hapus</a>
                        </td>
                     </tr>
         <?php
        	 $i++;
           } ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th style="width: 10px;">No</th>
                        <th>Nama</th>
                        <th style="width: 20px;">Shift</th>
                        <th>Mac address</th>
                        <th>Email</th>
                        <th>No. HP</th>
                        <th class="hide">Jatah Cuti</th>
                        <th>Pengaturan</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
</div> <!-- /.col-xs -->

<!-- Modal 1 -->
<div class="modal fade modal-edit" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4><b>Edit Data Karyawan</b></h4>
      </div>

      <div class="modal-body">
      <div id="box-edit" class="box box-info ">
          <div class="box-body">
          <form >
            <div class="alert-gagal"></div> 
          <div class="row">
            <div class="col-md-6">
              <input type="hidden" value="" name="id-karyawan" id="id-kar">

            <div class="form-group has-feedback">
              <label>Nama :</label>
              <input id="nama" type="email" class="form-control" placeholder="Nama Lengkap">
              <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>

            <div class="form-group has-feedback">
              <label>Email :</label>
              <input id="email" type="email" class="form-control" placeholder="Email">
              <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>

            <div class="form-group has-feedback">
              <label>Password Baru :</label>
              <input id="password" type="password" class="form-control" placeholder="(Kosongkan jika tidak berubah)">
              <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>

            <div class="form-group has-feedback">
              <label>MAC Address :</label>
              <input id="mac" type="text" class="form-control" maxlength = 17 placeholder="MAC Address">
              <span class="glyphicon glyphicon-phone form-control-feedback"></span>
            </div>
            </div><!--.col-md-->
            <div class="col-md-6">
              <div class="form-group has-feedback">
              <label>No. HP :</label>
              <input id="telepon" type="text" class="form-control" maxlength = 13 placeholder="No. HP">
              <span class="glyphicon glyphicon-phone form-control-feedback"></span>
            </div>

            <div class="form-group has-feedback">
              <label>Jatah Cuti :</label>
              <input id="jatah_cuti" type="number" class="form-control" maxlength = 2 placeholder="Jatah Cuti">
              <span class="glyphicon glyphicon-file form-control-feedback"></span>
            </div>

            <div class="form-group has-feedback">
              <label>Shift Kerja :</label>
              <select id="shift" class="form-control" style="width: 100%;">
                <option value="0">Shift 0 <small>( 08:00 - 17:00 )</small></option>
                <option value="1">Shift 1 <small>( 08:00 - 16:00 )</small></option>
                <option value="2">Shift 2 <small>( 13:00 - 21:00 )</small></option>
              </select>
            </div><!-- /.form-group -->

            <div class="form-group has-feedback">
              <label>Admin GePresence :</label>
              <select id="admin" class="form-control" style="width: 50%;">
                <option value="0">Tidak</option>
                <option value="1">Ya</option>
              </select>
            </div><!-- /.form-group -->

            </div><!--.col-md-->
          </div><!--.row -->
          </form>
          </div><!--.box-body-->
        </div><!--.box-->
      </div><!-- Modal-body-->

      <div class="modal-footer">
          <button type="button" class="btn btn-info simpan-edit"><b>SIMPAN</b></button>
          <button id="batal" type="button" class="btn" data-dismiss="modal"><b>BATAL</b></button>
      </div>

    </div><!--.modal-content-->
  </div><!--.modal-dialog -->
</div><!-- Modal 1 -->

  <!-- Modal 2 -->
 <div class="modal fade" id="modal-tambah" role="dialog">
    <div class="modal-dialog modal-sm">
     <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
          <h4><b>Tambah Data Karyawan</b></h4>
        </div>
        <div class="modal-body">
			<div id="box-tambah" class="box box-danger ">
			<div class="box-body">
				<form >
				<div class="alert-gagal"></div>
		          <div class="form-group has-feedback">
                <label>Nama :</label>
		            <input id="nama-tk" type="email" class="form-control" placeholder="Nama Lengkap">
		            <span class="glyphicon glyphicon-user form-control-feedback"></span>
		          </div>

		          <div class="form-group has-feedback">
		            <label>Email :</label>
                <input id="email-tk" type="email" class="form-control" placeholder="Email">
		            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
		          </div>

		          <div class="form-group has-feedback">
		            <label>Password :</label>
                <input id="password-tk" type="password" class="form-control" placeholder="Password">
		            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
		          </div>

		          <div class="form-group has-feedback">
		            <label>Mac Address :</label>
                <input id="mac2" type="text" class="form-control" maxlength = 17 placeholder="Android Mac Address">
		            <span class="glyphicon glyphicon-phone form-control-feedback"></span>
		          </div>
				
				  <div class="form-group has-feedback">
		            <label>No. HP :</label>
                <input id="telepon-tk" type="text" class="form-control" maxlength = 13 placeholder="No. HP">
		            <span class="glyphicon glyphicon-phone form-control-feedback"></span>
		          </div>
				</form>
			</div>
	  		</div>
        </div>
        <div class="modal-footer">
          <button id="simpan-tambah" type="button" class="btn btn-danger"><b>SIMPAN</b></button>
          <button id="batal" type="button" class="btn" data-dismiss="modal"><b>BATAL</b></button>
        </div>
      </div>
    </div>
  </div><!-- Modal 2 -->

<!-- page script -->
<script type="text/javascript">
      $(function () {
        $("#data-kar").DataTable({
          "paging": false,
          "lengthChange": true,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": true
        });
      });
// <!-- tambah : pada input mac address -->
	   length=1;
	 $('#mac').focusin(function (evt) {  
	    $(this).keypress(function () {
	        content=$(this).val();
	        content1 = content.replace(/\:/g, '');
	        length=content1.length;
	        if(((length % 2) == 0) && length < 12 && length > 1){
	            $('#mac').val($('#mac').val() + ':');
	            }
	   		 });
	 	});
//hapus data karyawan
	$('.hapus-kar').on('click', function () {
				var id=$(this).attr('data-id');
                            $.confirm({
                                title: 'Hapus data?',
                                content: false,
                                confirmButton: 'OK',
                                cancelButton: 'Batal',
                                confirmButtonClass: 'btn-danger',
                                animation: 'zoom',
                                confirm: function () {
                                    $.ajax({
                                    	type: "POST",
                                    	url: "action/hapus-karyawan.php",
                                    	data: "id="+id,
                                    	success : function(data){
                                    		if (data=='true') {
                                    			// window.location="home.php?page=dk";
                                          $('#judul').html("Data Karyawan Geschool");
                                          $('#main-content').load('datakaryawan.php');
                                    		} else {
                                    			$.alert({
                                    				title:false,
                                    				content: "Hapus data gagal!"
                                    			});
                                    		}
                                    	}
                                    });
                                }
                            });
                        });
	$('button').click(function(){ $(".alert").remove(); $(".overlay").remove();});
	// action tambah karyawan
    $('#simpan-tambah').click(function(event) {
        // Stop form from submitting normally
        event.preventDefault();
        // Get some values from elements on the page:
        var n = $("#nama-tk").val();
        var p = $("#password-tk").val();
      	var e = $("#email-tk").val();
      	var m = $("#mac2").val();
      	var t = $("#telepon-tk").val();
        $.ajax({
          type  : "POST",
          url   : "action/tambah-karyawan.php",
          data  : "email="+e+"&password="+p+"&nama="+n+"&mac="+m+"&telepon="+t,
          success : function(html){
			     if (html=='true') {
               load_page('dk');
               $('.modal-backdrop').remove();

            } else if(html=='duplikasi'){
 				        $(".overlay").remove();
                $(".alert").remove();
                $("#alert-gagal").append("<div class='alert alert-danger alert-dismissable'><button class='close' aria-hidden='true' type='button' data-dismiss='alert'>×</button><h5><i class='icon fa fa-ban'></i>Data pernah digunakan!</h5></div>");
            } else if (html=='false') {
                $(".overlay").remove();
                $(".alert").remove();
                $("#alert-gagal").append("<div class='alert alert-danger alert-dismissable'><button class='close' aria-hidden='true' type='button' data-dismiss='alert'>×</button><h5><i class='icon fa fa-ban'></i>Data tidak lengkap!</h5></div>");
            } else {
            	  $(".overlay").remove();
                $(".alert").remove();
                $("#alert-gagal").append("<div class='alert alert-danger alert-dismissable'><button class='close' aria-hidden='true' type='button' data-dismiss='alert'>×</button><h5><i class='icon fa fa-ban'></i>ERROR!</h5></div>");
            }
          },
          beforeSend:function(){
            $("#box-tambah").append("<div class='overlay'> <!-- Loading --> <i class='fa fa-refresh fa-spin'></i></div>");
          }
        });
        return false;
    });
	// action tambah karyawan
    $('.simpan-edit').click(function(event) {
        // Stop form from submitting normally
        event.preventDefault();
        // Get some values from elements on the page:
        var id = $("#id-kar").val();
        var n = $("#nama").val();
        var p = $("#password").val();
    		var e = $("#email").val();
    		var m = $("#mac").val();
    		var t = $("#telepon").val();
        var s = $("#shift option:selected").val();
        var a = $("#admin option:selected").val();
    		var j = $("#jatah_cuti").val();
        $.ajax({
          type  : "POST",
          url   : "action/edit-karyawan.php",
          data  : "email="+e+"&password="+p+"&nama="+n+"&mac="+m+"&telepon="+t+"&id="+id+"&jatah="+j+"&shift="+s+"&admin="+a,
          success : function(html){
           if (html=='true') {
                load_page('dk');
                $('.modal-backdrop').remove();
            } else {
 				        $(".overlay").remove();
                $(".alert").remove();
                $("#alert-gagal").append("<div class='alert alert-danger alert-dismissable'><button class='close' aria-hidden='true' type='button' data-dismiss='alert'>×</button><h5><i class='icon fa fa-ban'></i>Pembaruan data tidak berhasil!</h5></div>");
            }
          },
          beforeSend:function(){
            $("#box-edit").append("<div class='overlay'> <!-- Loading --> <i class='fa fa-refresh fa-spin'></i></div>");
          }
        });
        return false;
    });
</script>
<script type="text/javascript">
	// <!-- tambah : pada input mac address -->
	 length=1;
	 $('#mac2').focusin(function (evt) {  
	    $(this).keypress(function () {
	        content=$(this).val();
	        content1 = content.replace(/\:/g, '');
	        length=content1.length;
	        if(((length % 2) == 0) && length < 12 && length > 1){
	            $('#mac2').val($('#mac2').val() + ':');
	            }
	   		 });
	 	});
	 //input td parameter ke dalam modal
	 $('.edit-kar').click(function(){
	 	var id=$(this).attr('data-idkaryawan');
	 	var nama=$(this).closest('tr').find('.td_nama').attr('data-nama');
	 	var email=$(this).closest('tr').find('.td_email').text();
	 	var telepon=$(this).closest('tr').find('.td_telepon').text();
	 	var mac=$(this).closest('tr').find('.td_mac').text();
    var sh=$(this).closest('tr').find('.td_shift').text();
    var ad=$(this).closest('tr').find('.td_email').attr('data-admin');
	 	var jatah=$(this).closest('tr').find('.td_jatah').text();
	 	
    $('#id-kar').val(id);
	 	$('#nama').val(nama);
		$('#email').val(email);
		$('#mac').val(mac);
		$('#telepon').val(telepon);
		$('#jatah_cuti').val(jatah);	 	
    $("#shift option").filter(function() {
    return $(this).val() == sh; 
    }).prop('selected', true);
    $("#admin option").filter(function() {
    return $(this).val() == ad; 
    }).prop('selected', true);

	 	$('.modal-edit').modal({
	 		backdrop:'static',
	 		show:true
	 	});
	 });
</script>
<?php
} else {
 	header("location:index.php");
 }
 ?>