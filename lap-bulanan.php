<?php 
session_start();
error_reporting(0);

require_once 'include/db_function.php';
$db= new db_function();

//menampilkan bulan dalam format angka
$date = getdate();
$month = $date['mon'];

if (isset($_SESSION['admin'])||isset($_SESSION['tamu'])) {
 ?>
<!-- Main content -->
<div class="row">
  <section class="invoice">
  <!-- title row -->
  <div class="row">
    <div class="col-md-10">
      <h2 class="page-header">
        <i class="fa fa-globe"></i> PT. Geschool Cerdas Mandiri
      </h2>
    </div><!-- /.col -->
    <div class="col-md-2">
    <div class="form-group pull-right no-print" style="width:200px">
                    <select class="form-control" style="width: 100%;">
                        <option data-id="<?php echo $_REQUEST['id']; ?>" data-b="<?php echo $month; ?>" data-t="<?php echo date('Y'); ?>">-- Pilih Bulan --</option>
                    <?php 
                      $bl=$db->getBulanTahun();
                      foreach ($bl as $s) { ?>
                         <option data-id="<?php echo $_REQUEST['id']; ?>" data-b="<?php echo $s['bulan']; ?>" data-t="<?php echo $s['tahun']; ?>"><?php echo $db->namaBulan($s['bulan'])." - ".$s['tahun']; ?></option> 
                       <?php 
                        } ?>
                    </select>                   
                  </div><!-- /.form-group -->
    </div>
  </div> <!-- .row-->
  <!-- info row -->
  <div class="row invoice-info">
    <div class="col-sm-4 invoice-col">
      From :
      <address>
        <strong>Geschool Admin</strong><br>
        Jl. Timoho no.67, Gondokusuman<br>
        Kota Yogyakarta<br>
        Phone : (0274) 633181<br>
        Email : admin@geschool.net
      </address>
    </div><!-- /.col -->
    <div class="col-sm-4 invoice-col">
      To :
<?php 
    $data=$db->getUser($_REQUEST['id']);
 ?>

      <address>
        <strong><?php echo ucwords($data['nama']); ?></strong><br>
        Phone : <?php echo $data['telepon']; ?><br>
        Email : <?php echo $data['email']; ?>
      </address>
    </div><!-- /.col -->
    <div class="col-sm-4 invoice-col">
      <b>Date : </b><?php echo date('d-m-Y'); ?><br>
    </div><!-- /.col -->
  </div><!-- /.row -->

  <!-- Table row -->
  <div class="row">
    <div class="col-xs-12 table-responsive" id="tabel-bulanan">
      <p class="lead">Presensi - <?php echo $db->namaBulan($_REQUEST['bulan'])." ".$_REQUEST['tahun']; ?></p>
      <table class="table table-striped">
        <thead>
          <tr>
            <th style="width: 10px;">No</th>
            <th>Tanggal</th>
            <th>Datang</th>
            <th>Pulang</th>
            <th>Koreksi D</th>
            <th>Koreksi P</th>
            <th>Durasi Kerja</th>
          </tr>
        </thead>
        <tbody>
  <?php 

  $pb=$db->presensiBulanan($_REQUEST['id'],$_REQUEST['bulan'],$_REQUEST['tahun']);
  $iz=$db->izinBulanan($_REQUEST['id'],$_REQUEST['bulan'],$_REQUEST['tahun']);

  $i=1; $telat=0; $tjk=0; $tmo=0;
  foreach ($pb as $value) {

    $hh=(int)substr($value['jam_datang'],11,2);
    $mm=(int)substr($value['jam_datang'],14,2);
    $hk=(int)substr($value['koreksi_jd'],11,2);
    $mk=(int)substr($value['koreksi_jd'],14,2);
    
    $jk=(int)substr($value['jumlah_jam'],0,2);
    $mo=(int)substr($value['jumlah_jam'],3,2);

    //Hitung jumlah terlambat kerja
    if($value['shift']==0 || $value['shift']==1){
      if ($hk!=00){
        if ($hk>=8 && $mk>=5) { $telat++; }
      } else { 
        if ($hh>=8 && $mm>=5) { $telat++; }
      }  
    } else {
      if ($hk!=00){
        if ($hk>=13 && $mk>=5) { $telat++; }
      } else { 
        if ($hh>=13 && $mm>=5) { $telat++; }
      }
    }
    

    $tjk=$tjk+$jk;//hitung jumlah jam kerja
    $tmo=$tmo+$mo;//hitung jumlah menit kerja
   ?>
          <tr <?php 
            //Tandai hari yang melakukan izin
        foreach ($iz as $v){
          if ($value['tanggal']==$v['tanggal']) { echo " class=\" bg-yellow disabled\" ";} 
            }//.foreach ?> >
            <td><?php echo $i; ?></span></td>
            <td><?php echo $value['tanggal']; ?></td>
            <td><?php echo substr($value['jam_datang'],11); ?></td>
            <td><?php echo substr($value['jam_pulang'],11); ?></td>
            <td><?php echo substr($value['koreksi_jd'],11); ?></td>
            <td><?php echo substr($value['koreksi_jp'],11); ?></td>
            <td 
            <?php if ($value['jumlah_jam']=='00:00:00') { echo " class=\" bg-red disabled\" ";}  ?>
            ><?php echo $value['jumlah_jam']; ?></td>
          </tr>
<?php
 $i++; 
} 
  $tmo_j=floor($tmo / 60);
  
  if ($tmo>=60) {
      $tmo_m=$tmo % 60;
  } else { $tmo_m=$tmo;}

?>
        </tbody>
        <tfoot>
          <tr>
            <th></th>
            <th><span class="label label-warning">Ada izin</span></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th><span class="label label-danger">Presensi kurang</span></th>
          </tr>
        </tfoot>
      </table>
    </div><!-- /.col -->
  </div><!-- /.row -->

  <div class="row">
    <!-- accepted payments column -->
    <div class="col-xs-8">
      <p class="lead">Izin atau Cuti</p>
        <div class="table-responsive">
      <table class="table table-striped">
        <thead>
          <tr>
            <th style="width: 10px;">No</th>
            <th>Tanggal</th>
            <th>Pergi</th>
            <th>Kembali</th>
            <th>Durasi</th>
          </tr>
        </thead>
        <tbody>
  <?php 

  $a=1; $tji=0; $tmi=0;
  foreach ($iz as $value) {
    $ji=(int)substr($value['jumlah_jam'],0,2);
    $mi=(int)substr($value['jumlah_jam'],3,2);
    
    $tji=$tji+$ji; //hitung total jam izin
    $tmi=$tmi+$mi; //hitung total menit izin
   ?>
          <tr>
            <td><?php echo $a; ?></td>
            <td><?php echo $value['tanggal']; ?></td>
            <td><?php echo $value['jam_pergi']; ?></td>
            <td><?php echo $value['jam_kembali']; ?></td>
            <td><?php echo $value['jumlah_jam']; ?></td>
          </tr>
<?php $a++; } 

  $tmi_j=floor($tmi / 60);
  
  if ($tmi>=60) {
      $tmi_m=$tmi % 60;
  } else { $tmi_m=$tmi;}
?>
        </tbody>
      </table>
    </div><!-- /.table-responsive -->

    </div><!-- /.col -->
    <div class="col-xs-"4>
      <p class="lead">Keterangan</p>
      <div class="table-responsive">
        <table class="table">
         <tr>
            <th style="width:50%">Terlambat</th>
            <td>: <?php echo $telat; ?></td>
          </tr>
          <tr>
            <th>Entri Presensi</th>
            <td>: <?php echo ($i-1)." Hari"; ?></td>
          </tr>
          <tr>
            <th>Tot. Jam Kerja</th>
            <td>: <?php echo ($tjk+$tmo_j)." jam ".$tmo_m." menit"; ?></td>
          </tr>
          <tr>
            <th>Entri Izin</th>
            <td>: <?php echo ($a-1)." Kali"; ?></td>
          </tr>
          <tr>
            <th>Tot. Jam Izin</th>
            <td>: <?php echo ($tji+$tmi_j)." jam ".$tmi_m." menit"; ?></td>
          </tr>
        </table>
      </div>
    </div><!-- /.col -->
  </div><!-- /.row -->

  <!-- this row will not appear when printing -->
  <div class="row no-print">
    <div class="col-xs-12">
     <?php if ($_SESSION['admin']!="tamu") { ?>
      <a href="lap-bulanan-print.php?id=<?php echo $_REQUEST['id']; ?>&bulan=<?php echo $_REQUEST['bulan']; ?>&tahun=<?php echo $_REQUEST['tahun']; ?>" target="_blank" class="btn btn-success pull-right" style="margin-right: 5px;"><i class="fa fa-print"></i> Print</a>
     <?php } ?>
    </div>
  </div>
</section><!-- /.content -->
<div class="clearfix"></div>
</div>

<script type="text/javascript">
  $(document).ready(function(){

    $("select").change(function(){
    var id=$("select option:selected").attr('data-id');
    var bl=$("select option:selected").attr('data-b');
    var th=$("select option:selected").attr('data-t');
    jQuery.ajax({
      url : "lap-bulanan.php",
      data: "id="+id+"&bulan="+bl+"&tahun="+th,
      type: "POST",
      success : function(data){
        $("#main-content").html(data);
      }
       });
    });

    //batas bawah $(document).ready()
  });
</script>
<?php 
  } 
?>