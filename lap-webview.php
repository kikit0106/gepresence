<?php 
error_reporting(0);
//menampilkan bulan dalam format angka
$date = getdate();
$month = $date['mon'];

require_once 'include/db_function.php';
$db= new db_function(); ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>GePresence | Invoice</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="template/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="template/dist/css/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="template/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="template/dist/css/skins/_all-skins.min.css">
  </head>

  <body>
     <!-- Main content -->
    <div class="wrapper">
      <!-- title row -->
      <div class="row">
        <div class="box">
          <div class="box-header">
      <div class="col-xs-5">
        <h4 class="box-title"> Invoice </h4>
      </div>
      <div class="col-xs-1"></div>
      <div class="col-xs-6">
           <div class=" form-group">
            <select class="form-control" style="width: 100%;">
                <option data-id="<?php echo $_REQUEST['id']; ?>" data-b="<?php echo $month; ?>" data-t="<?php echo date('Y'); ?>">-- Pilih Bulan --</option>
            <?php 
              $bl=$db->getBulanTahun();
              foreach ($bl as $s) { ?>
                 <option data-id="<?php echo $_REQUEST['id']; ?>" data-b="<?php echo $s['bulan']; ?>" data-t="<?php echo $s['tahun']; ?>"><?php echo $db->namaBulan($s['bulan'])." - ".$s['tahun']; ?></option> 
               <?php 
                } ?>
            </select>                   
          </div><!-- /.form-group -->
      </div>
          </div>
        </div>    
      </div> <!-- .row-->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title"> Presensi - <?php $db->namaBulan($_REQUEST['bulan'])." ".$_REQUEST['tahun']; ?></h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <div class="box-body table-responsive">
              <table class="table table-striped">
            <thead>
              <tr>
                <th style="width:10px;">Tgl</th>
                <th>Datang</th>
                <th>Pulang</th>
                <th>Koreksi D</th>
                <th>Koreksi P</th>
                <th>Durasi</th>
              </tr>
            </thead>
            <tbody>
      <?php 
      $pb=$db->presensiBulanan($_REQUEST['id'],$_REQUEST['bulan'],$_REQUEST['tahun']);

      $i=1; $telat=0; $tjk=0; $tmo=0;
      foreach ($pb as $value) {

        $hh=(int)substr($value['jam_datang'],11,2);
        $mm=(int)substr($value['jam_datang'],14,2);
        $hk=(int)substr($value['koreksi_jd'],11,2);
        $mk=(int)substr($value['koreksi_jd'],14,2);
        
        $jk=(int)substr($value['jumlah_jam'],0,2);
        $mo=(int)substr($value['jumlah_jam'],3,2);

        //Hitung jumlah terlambat kerja
        if($value['shift']==0 || $value['shift']==1){
          if ($hk!=00){
            if ($hk>=8 && $mk>=5) { $telat++; }
          } else { 
            if ($hh>=8 && $mm>=5) { $telat++; }
          }  
        } else {
          if ($hk!=00){
            if ($hk>=13 && $mk>=5) { $telat++; }
          } else { 
            if ($hh>=13 && $mm>=5) { $telat++; }
          }
        }

        $tjk=$tjk+$jk;//hitung jumlah jam kerja
        $tmo=$tmo+$mo;//hitung jumlah menit kerja
       ?>
              <tr>
                <td><?php echo substr($value['tanggal'],8); ?></td>
                <td><?php echo substr($value['jam_datang'],11); ?></td>
                <td><?php echo substr($value['jam_pulang'],11); ?></td>
                <td><?php echo substr($value['koreksi_jd'],11); ?></td>
                <td><?php echo substr($value['koreksi_jp'],11); ?></td>
                <td><?php echo $value['jumlah_jam']; ?></td>
              </tr>
    <?php $i++; } 
      $tmo_j=floor($tmo / 60);
      
      if ($tmo>=60) {
          $tmo_m=$tmo % 60;
      } else { $tmo_m=$tmo;}

    ?>
            </tbody>
          </table>
            </div>
          </div>
        </div><!-- /.col -->
      </div><!-- /.row -->

      <div class="row">
        <!-- izin & cuti column -->
        <div class="col-xs-12 col-sm-6">
         <div class="box box-warning">
           <div class="box-header">
              <h3 class="box-title">Izin atau Cuti</h3>
           </div>
           <div class="box-body table-responsive">
             <table class="table table-striped">
            <thead>
              <tr>
                <th style="width:10px;">Tgl</th>
                <th>Pergi</th>
                <th>Kembali</th>
                <th>Durasi</th>
              </tr>
            </thead>
            <tbody>
      <?php 
      $iz=$db->izinBulanan($_REQUEST['id'],$_REQUEST['bulan'],$_REQUEST['tahun']);

      $a=1; $tji=0; $tmi=0;
      foreach ($iz as $value) {
        $ji=(int)substr($value['jumlah_jam'],0,2);
        $mi=(int)substr($value['jumlah_jam'],3,2);
        
        $tji=$tji+$ji; //hitung total jam izin
        $tmi=$tmi+$mi; //hitung total menit izin
       ?>
              <tr>
                <td><?php echo substr($value['tanggal'],8); ?></td>
                <td><?php echo substr($value['jam_pergi'],11); ?></td>
                <td><?php echo substr($value['jam_kembali'],11); ?></td>
                <td><?php echo $value['jumlah_jam']; ?></td>
              </tr>
    <?php $a++; } 

      $tmi_j=floor($tmi / 60);
      
      if ($tmi>=60) {
          $tmi_m=$tmi % 60;
      } else { $tmi_m=$tmi;}
    ?>
            </tbody>
          </table>
           </div>
         </div>
        </div><!-- /.col -->

        <div class="col-xs-12 col-sm-6">
          <div class="box box-success">
            <div class="box-header">
              <h3 class="box-title">Keterangan</h3>
            </div>
            <div class="box-body table-responsive">
                          <table class="table">
              <tr>
                <th style="width:50%">Terlambat</th>
                <td>: <?php echo $telat; ?></td>
              </tr>
              <tr>
                <th>Entri Presensi</th>
                <td>: <?php echo ($i-1)." Hari"; ?></td>
              </tr>
              <tr>
                <th>Tot. Jam Kerja</th>
                <td>: <?php echo ($tjk+$tmo_j)." j : ".$tmo_m." m"; ?></td>
              </tr>
              <tr>
                <th>Entri Izin</th>
                <td>: <?php echo ($a-1)." Kali"; ?></td>
              </tr>
              <tr>
                <th>Tot. Jam Izin</th>
                <td>: <?php echo ($tji+$tmi_j)." j : ".$tmi_m." m"; ?></td>
              </tr>
            </table>
            </div>
          </div>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="template/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="template/bootstrap/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="template/dist/js/app.min.js"></script>
    <script type="text/javascript">
      $(document).ready(function(){

        $("select").change(function(){
        var id=$("select option:selected").attr('data-id');
        var bl=$("select option:selected").attr('data-b');
        var th=$("select option:selected").attr('data-t');
        jQuery.ajax({
          url : "lap-webview.php",
          data: "id="+id+"&bulan="+bl+"&tahun="+th,
          type: "POST",
          success : function(data){
            $(".wrapper").html(data);
          }
           });
        });

      });
    </script>
  </body>
</html>