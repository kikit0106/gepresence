$(document).ready(function() {
        // when the user submit a form
        $('form').submit(function(event) {
        // Stop form from submitting normally
        event.preventDefault();
        // Get some values from elements on the page:
        var e = $("#akun_email").val();
        var p = $("#akun_pass").val();
        $.ajax({
          type  : "POST",
          url   : "action/cek-login-admin.php",
          data  : "email="+e+"&password="+p,
          success : function(data){
            if (data=='true') {
              window.location="home.php";
            } else if(data=='tamu') {
              window.location="tamu.php";
            } else {
              $(".overlay").remove();
              $(".alert").remove();
              $("#login-box").append("<div class='alert alert-danger alert-dismissable'><button class='close' aria-hidden='true' type='button' data-dismiss='alert'>×</button><h5><i class='icon fa fa-ban'></i>Email atau password salah!</h5></div>");
              setTimeout(function(){
                $(".alert").fadeOut();        
              }, 3000); 
            }
          },
          beforeSend:function(){
            $("#box").append("<div class='overlay'> <!-- Loading --> <i class='fa fa-refresh fa-spin'></i></div>");
          }
        });
        return false;
        });
});