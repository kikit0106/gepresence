<?php 
session_start();
error_reporting(0);

require_once 'include/db_function.php';
$db= new db_function();
//menampilkan bulan dalam format angka
$date = getdate();
$month = $date['mon'];

if (isset($_SESSION['admin'])||isset($_SESSION['tamu'])) {
?>
<div class="col-md-8">
<div class="box box-warning">
                <div class="box-header">
                  	<h4 class="box-title">Laporan Harian</h4>
            					<div class="box-tools pull-right">
            					   <div class="input-group" style="width: 171px">
            					    <div class="input-group-btn">
                            <a id="lihat-lap" class="btn btn-warning btn-flat ">Lihat</a>
                          </div>
                           <input type="text" style="width:133px;" class="form-control" id="tanggal" value="<?php echo date('Y-m-d'); ?>" >
                        </div><!-- /.input group -->
            					</div>
        			 
                </div><!-- /.box-header -->
                <div class="box-body" id="content-lap">
                	
                <table id="data-presensi" class="table table-bordered table-striped table-hover">
                    <thead>
                      <tr>
                        <th style="width: 10px;">No</th>
                        <th style="width: 175px;">Nama</th>
                        <th>Datang</th>
                        <th>Pulang</th>
                        <th>Koreksi D</th>
                        <th>Koreksi P</th>
                      </tr>
                    </thead>
                    <tbody>
         <?php 
         	$tanggal=date('Y-m-d');
    			$data=$db->laporanHarian($tanggal);
         
          $i=1;
          foreach ($data as $value) {
					 ?>
					 <tr>
              <td><?php echo $i; ?></td>
              <td class="td_nama"><span><?php echo ucwords($value['nama'])."</span> - ".$value['shift']; ?></td>
              <td>
              <?php if($value['TIME(presensi.jam_datang)']!="00:00:00") {
                $db->cekDatang($value['TIME(presensi.jam_datang)'],$value['shift']);
                } ?>
              </td>
              <td>
              <?php if($value['TIME(presensi.jam_pulang)']!="00:00:00") {
                $db->cekPulang($value['TIME(presensi.jam_pulang)'],$value['shift']);
                } ?>
              </td>
              <td>
              <?php if($value['TIME(presensi.koreksi_jd)']!="00:00:00") {
                  echo "<span class=\"badge bg-blue\">".$value['TIME(presensi.koreksi_jd)']."</span>";
                } ?>
              </td>
              <td>
              <?php if($value['TIME(presensi.koreksi_jp)']!="00:00:00") {
                  echo "<span class=\"badge bg-blue\">".$value['TIME(presensi.koreksi_jp)']."</span>";
                } ?>
              </td>
           </tr>
         <?php
        	 $i++;} ?>
                    </tbody>
                </table>
                </div><!-- /.box-body -->
          <div class="box-footer">
            <?php if (isset($_SESSION['admin'])) { ?>
              <button id="bt-koreksi" class="btn pull-right" style="margin-right: 5px;" data-toggle="modal" data-target="#modal-koreksi" data-backdrop="static">
              <i class="fa fa-edit"></i>
              Koreksi Presensi
              </button>
            <?php } ?>
          </div>
 </div><!-- /.box -->
</div><!-- .col-->
<div class="col-md-4">
		<div class="box box-warning">
              <div class="box-header with-border">
                  <h4 class="box-title">Laporan Bulanan</h4>
                  <div class="box-tools pull-right">
            		<button title="Collapse" class="btn btn-box-tool" data-toggle="tooltip" data-widget="collapse"><i class="fa fa-minus"></i></button>
        			</div>
               </div>
               <div class="box-body">
                  <table id="data-kar" class="table table-bordered table-striped table-hover">
                    <thead>
                      <tr>
                        <th>Nama</th>
                        <th style="width: 40px;">Detail</th>
                      </tr>
                    </thead>
                    <tbody>
         <?php 
         $d=$db->getAllUser();
         $a=0;
         foreach ($d as $value) {
          $a++;
					 ?>
					<tr>
                        <td>
                          <?php echo ucwords($value['nama']); ?>                         
        <?php
            $cek=true;
            foreach ($data as $cari){ if ($cari['nama']==$value['nama']) $cek=false; } 
                  if ($cek) {
                    echo   "<small class='label label-warning pull-right' title='Belum presensi' data-toggle='tooltip'><i class='fa fa-warning'></i></small>"; }
          ?>
                        </td>
                        <td>
                        	<a class="badge bg-green pull-left detail" data-id="<?php echo $value['id_karyawan']; ?>" data-b="<?php echo $month; ?>" data-t="<?php echo date('Y'); ?>">Detail</a>  
                        </td>
                     </tr>
         <?php
        	} ?>
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
	      </div><!-- /. box -->
</div><!-- .col-->

  <!-- Modal 3 -->
 <div class="modal fade" id="modal-koreksi" role="dialog">
    <div class="modal-dialog modal-sm">
     <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
          <h4><b>Koreksi Jam Presensi</b></h4>
        </div>
        <div class="modal-body">
      <div id="box-tambah" class="box box-warning ">
      <div class="box-body">
        <form >
        <div class="alert-gagal"></div>

            <div class="form-group">
              <label>Kondisi</label>
              <select id="kondisi" class="form-control" style="width: 100%;">
                <option data="datang">Datang</option>
                <option data="pulang">Pulang</option>
              </select>
            </div><!-- /.form-group -->
        
        <div class="form-group">
            <label>Nama Karyawan</label>
            <select id="nama-kar" class="form-control select2" style="width: 100%;">
    <?php 
      foreach ($d as $v) { ?>
              <option data-id="<?php echo $v['id_karyawan']; ?>"><?php echo ucwords($v['nama']); ?></option>
      <?php } ?>
            </select>
          </div><!-- /.form-group -->

          <div class="bootstrap-timepicker">
              <div class="form-group">
                <label>Jam</label>
                <div class="input-group">
                  <input id="jam" type="text" class="form-control timepicker">
                  <div class="input-group-addon">
                    <i class="fa fa-clock-o"></i>
                  </div>
                </div><!-- /.input group -->
              </div><!-- /.form group -->
          </div>

        </form>
      </div>
        </div>
        </div>
        <div class="modal-footer">
          <button id="simpan-koreksi" class="btn btn-warning"><b>SIMPAN</b></button>
          <button id="batal" class="btn" data-dismiss="modal"><b>BATAL</b></button>
        </div>
      </div>
    </div>
  </div><!-- Modal 3 -->

<script type="text/javascript">
$(document).ready(function(){

	$(function () {
	    $("#data-presensi").DataTable({
	      "paging": false,
	      "lengthChange": true,
	      "searching": true,
	      "ordering": true,
	      "info": true,
	      "autoWidth": true
	    	});
  });

  $(".select2").select2();

	$("#tanggal").datepicker({
		format:"yyyy-mm-dd"
    });

	$("#lihat-lap").click(function(){
    loadPresensi();
	});

	$(".detail").click(function(){
    var id=$(this).attr('data-id');
    var bl=$(this).attr('data-b');
    var th=$(this).attr('data-t');
		jQuery.ajax({
      url : "lap-bulanan.php",
      data: "id="+id+"&bulan="+bl+"&tahun="+th,
      type: "POST",
      success : function(data){
        $('#judul').html("Laporan per Bulan");
        $("#main-content").html(data);
      }
      });
	 });

  $(".timepicker").timepicker({
          showInputs: false,
          showMeridian : false,
          showSeconds : true
        });
<?php if (isset($_SESSION['admin'])) { ?>
  //Koreksi jam
  $("#simpan-koreksi").click(function(){
    var k =$("#kondisi option:selected").attr("data");
    var id=$("#nama-kar option:selected").attr("data-id");
    var j =$("#jam").val();
    var tgl=$("#tanggal").val();
    var koreksi = tgl+" "+j ;

    jQuery.ajax({
      url  : "action/koreksi-jam.php",
      data : "id="+id+"&kondisi="+k+"&jam="+koreksi,
      type : "POST",
      success : function(){
        loadPresensi();
        $('.modal').modal('toggle');
        $('.overlay').remove();
        $('.modal-backdrop').remove();
      },
      beforeSend:function(){
            $("#box-tambah").append("<div class='overlay'> <!-- Loading --> <i class='fa fa-refresh fa-spin'></i></div>");
          }
      });
    });
<?php } ?>

  function loadPresensi(){
    var tgl= $("#tanggal").val();

    jQuery.ajax({
      url : "lap-harian.php",
      data: "tanggal="+tgl,
      type: "POST",
      success : function(data){
        $("#content-lap").html(data); 
  // if (tgl!=now) { $("#bt-koreksi").hide();} 
  // else { $("#bt-koreksi").show();}
        }
    });
  }

});
</script>

<?php
} else {
 	header("location:index.php");
 }
 ?>