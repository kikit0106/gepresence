<?php 
	/**
	* 
	*/
	class  db_function
	{
		private $conn;

		//constructor
		function __construct()
		{
			require_once 'db_connect.php';
			//connecting to database
			$db = new db_connect();
			$this->conn =$db->connect();
		}

		//destructor
		function __destruct(){ 
		}

		/**
		*Storing new user
		*return user detail
		*/
		public function storeUser($nama,$email,$password,$telepon,$mac){
			
			$hash	= $this->hashSSHA($password);
			$pass 	= $hash['encrypted'];
			$salt	= $hash['salt'];

			$stmt	= $this->conn->prepare("INSERT INTO karyawan(nama,password,salt,mac_address,telepon,email,created_at) VALUES('$nama','$pass','$salt','$mac','$telepon','$email',NOW())");
			//$stmt->bind_param($nama,$pass,$salt,$mac,$telepon,$email);
			$result = $stmt->execute();
			$stmt->close();

			if($result){
				$stmt= $this->conn->prepare("SELECT * FROM karyawan WHERE email = '$email'");
				//$stmt->bind_param("s",$email);
				$stmt->execute();
				$user=$stmt->get_result()->fetch_assoc();
				$stmt->close();

				return $user;
			} else {
				return false;
			}
		}

		//Store permintaan izin ke database
		public function storeIzin($id_karyawan,$jp,$jk,$kp){

			$tgl=substr($jp,0,10);
			$stmt = $this->conn->prepare("INSERT INTO izin (`id_karyawan`, `tanggal`, `jam_pergi`, `jam_kembali`, `allday`, `keperluan`, `status_1`, `warna`,
`jumlah_jam`) VALUES ('$id_karyawan','$tgl','$jp','$jk',0,'$kp',0,'#00c0ef',TIMEDIFF('$jk','$jp'))");
			$result = $stmt->execute();
			$stmt->close();

			if($result){
				return true;
			} else {
				return false;
			}
		}

		//update data karyawan
		public function updateDataUser($id,$nama,$email,$telepon,$mac,$jatah,$shift,$jabatan){

			$stmt	= $this->conn->prepare("UPDATE karyawan SET nama='$nama',email='$email',telepon='$telepon',mac_address='$mac',jatah_cuti='$jatah',shift='$shift',jabatan='$jabatan' WHERE id_karyawan='$id'");

			$result = $stmt->execute();
			$stmt->close();

			if($result){
				return true;
			} else {
				return false;
			}
		}

		//update password karyawan
		public function updatePasswordUser($id,$password){

			$hash	= $this->hashSSHA($password);
			$pass 	= $hash['encrypted'];
			$salt	= $hash['salt'];
			
			$stmt	= $this->conn->prepare("UPDATE karyawan SET password='$pass',salt='$salt' WHERE id_karyawan='$id'");
			$result = $stmt->execute();
			$stmt->close();

			if($result){
				return true;
			} else {
				return false;
			}
		}
		
		//Delete data karyawan
		public function deleteUser($id){
			$stmt	= $this->conn->prepare("DELETE FROM karyawan WHERE id_karyawan='$id'");

			$result = $stmt->execute();
			$stmt->close();

			if($result){
				return true;
			} else {
				return false;
			}
		}

		//Get user by id
		public function getUser($idKaryawan){
			$stmt = $this->conn->prepare("SELECT * FROM karyawan WHERE id_karyawan='$idKaryawan'");
			//$stmt->bind_param("ss",$email,$mac);

			if($stmt->execute()){
				$user=$stmt->get_result()->fetch_assoc();
				$stmt->close();
				return $user;
			} else{
				return NULL;
			}
		}

		//Get user by email ,password and mac (login android)
		public function getUserData($email,$password,$mac){
			$stmt = $this->conn->prepare("SELECT * FROM karyawan WHERE email='$email' AND mac_address='$mac'");
			//$stmt->bind_param("ss",$email,$mac);

			if($stmt->execute()){
				$user=$stmt->get_result()->fetch_assoc();
				$stmt->close();
				return $user;
			} else{
				return NULL;
			}
		}
	
		//tampilkan seluruh data karyawan
		public function getAllUser()
		{
			$stmt = $this->conn->prepare("SELECT * FROM karyawan ORDER BY nama ASC");
			//$stmt->bind_param("ss",$email,$mac);

			if($stmt->execute()){
				$user=$stmt->get_result();
				$stmt->close();
				return $user;
			} else{
				return NULL;
			}
		}
		//check admin login
		public function loginAdmin($email,$password){
			$jabatan = 111;

			$stmt = $this->conn->prepare("SELECT * FROM karyawan WHERE email='$email' ");
			//$stmt->bind_param("ss",$email,$mac);

			if($stmt->execute()){
				$user=$stmt->get_result()->fetch_assoc();
				$stmt->close();
				
				$salt	=$user['salt'];
				$pass 	=$this->checkHashSSHA($salt,$password);
					
					if ($user['password']==$pass && $user['jabatan']==$jabatan) {
						return $user;
					} elseif ($user['password']==$pass) {
						return $user="tamu";
					} else {
						return NULL;
					}
			} else{
				return NULL;
			}
		}

		//Cek user if exist
		public function isUserExist($email,$mac,$telepon){
			$stmt = $this->conn->prepare("SELECT email FROM karyawan WHERE email = '$email' OR mac_address='$mac' OR telepon='$telepon'");
			//$stmt->bind_param("s",$email);
			$stmt->execute();
			$stmt->store_result();

			if($stmt->num_rows > 0){
				//row exist
				$stmt->close();
				return true;
			} else {
				//row not exist
				$stmt->close();
				return false;
			}
		}

		//check row
		private function isRowPresensiExist($idKaryawan,$tanggal){
			$stmt= $this->conn->prepare("SELECT * FROM presensi WHERE tanggal='$tanggal' AND id_karyawan={$idKaryawan}");
			$stmt->execute();
			$stmt->store_result();

			if($stmt->num_rows > 0){
				//user exist
				$stmt->close();
				return true;
			} else {
				//user not exist
				$stmt->close();
				return false;
			}
		}

		public function prepareJamDatang($idKaryawan,$jam){
			$tanggal=date('Y-m-d');
			
			if ($this->isRowPresensiExist($idKaryawan,$tanggal)) {
				if ($jam != "0000-00-00 00:00:00") {	
					$stmt=$this->conn->prepare("UPDATE presensi SET koreksi_jd='$jam' WHERE id_karyawan='$idKaryawan' AND tanggal=CURDATE()");
				} else {
					$stmt=$this->conn->prepare("UPDATE presensi SET jam_datang=NOW() WHERE id_karyawan='$idKaryawan' AND tanggal=CURDATE()");
				}
			} else {
				if ($jam != "0000-00-00 00:00:00") {
					$stmt=$this->conn->prepare("INSERT INTO presensi (`id_karyawan`, `tanggal`, `jam_datang`, `jam_pulang`, `koreksi_jd`, `koreksi_jp`, `jumlah_jam`, `keterangan`) VALUES ('$idKaryawan', CURDATE(), '', '', '$jam', '', '', '')");
				} else {
					$stmt=$this->conn->prepare("INSERT INTO presensi (`id_karyawan`, `tanggal`, `jam_datang`, `jam_pulang`, `koreksi_jd`, `koreksi_jp`, `jumlah_jam`, `keterangan`) VALUES ('$idKaryawan', CURDATE(), NOW(), '', '', '', '', '')");
				}
			}

			if($stmt->execute()){

				$this->setShift($idKaryawan);
				$this->hitungJumlahJam($idKaryawan,$tanggal);
				$this->hitungKeterangan($idKaryawan,$tanggal);
			
				$stmt->close();
				return true;
			} else {
				//user not exist
				$stmt->close();
				return false;
			}
		}

		public function prepareJamPulang($idKaryawan,$jam){
			$tanggal=date('Y-m-d');
	
			if ($this->isRowPresensiExist($idKaryawan,$tanggal)) {
				if ($jam != "0000-00-00 00:00:00") {
					$stmt=$this->conn->prepare("UPDATE presensi SET koreksi_jp='$jam' WHERE id_karyawan='$idKaryawan' AND tanggal=CURDATE()");
				} else {
					$stmt=$this->conn->prepare("UPDATE presensi SET jam_pulang=NOW() WHERE id_karyawan='$idKaryawan' AND tanggal=CURDATE()");
				} 
			} else {
				if ($jam != "0000-00-00 00:00:00") {
					$stmt=$this->conn->prepare("INSERT INTO presensi (`id_karyawan`, `tanggal`, `jam_datang`, `jam_pulang`, `koreksi_jd`, `koreksi_jp`, `jumlah_jam`, `keterangan`) VALUES ('$idKaryawan', CURDATE(), '', '', '', '$jam', '', '')");
				} else {
					$stmt=$this->conn->prepare("INSERT INTO presensi (`id_karyawan`, `tanggal`, `jam_datang`, `jam_pulang`, `koreksi_jd`, `koreksi_jp`, `jumlah_jam`, `keterangan`) VALUES ('$idKaryawan', CURDATE(), '', NOW(), '', '', '', '')");
				}
			}

			if($stmt->execute()){

				$this->setShift($idKaryawan);
				$this->hitungJumlahJam($idKaryawan,$tanggal);
				$this->hitungKeterangan($idKaryawan,$tanggal);
				$stmt->close();
		
				return true;
			} else {
				$stmt->close();
				return false;
			}
		}

		//update kamis,10-03-2016 "koreksi presensi"
		public function koreksiJam($k,$idKaryawan,$tanggal,$koreksi){
			if ($k=="datang") {

				if ($this->isRowPresensiExist($idKaryawan,$tanggal)) {	
					$stmt=$this->conn->prepare("UPDATE presensi SET koreksi_jd='$koreksi' WHERE id_karyawan='$idKaryawan' AND tanggal='$tanggal' ");
				} else {
					$stmt=$this->conn->prepare("INSERT INTO presensi (`id_karyawan`, `tanggal`, `jam_datang`, `jam_pulang`, `koreksi_jd`, `koreksi_jp`, `jumlah_jam`, `keterangan`) VALUES ('$idKaryawan', '$tanggal', '', '', '$koreksi', '', '', '')");
				}

			} elseif($k=="pulang"){
				
				if ($this->isRowPresensiExist($idKaryawan,$tanggal)) {
					$stmt=$this->conn->prepare("UPDATE presensi SET koreksi_jp='$koreksi' WHERE id_karyawan='$idKaryawan' AND tanggal='$tanggal' ");
				} else {
					$stmt=$this->conn->prepare("INSERT INTO presensi (`id_karyawan`, `tanggal`, `jam_datang`, `jam_pulang`, `koreksi_jd`, `koreksi_jp`, `jumlah_jam`, `keterangan`) VALUES ('$idKaryawan', '$tanggal', '', '', '', '$koreksi', '', '')");
				}

			} else { return false; }

			if($stmt->execute()){

				$this->setShift($idKaryawan);
				$this->hitungJumlahJam($idKaryawan,$tanggal);
				$this->hitungKeterangan($idKaryawan,$tanggal);
				$stmt->close();
		
				return true;
			} else {
				$stmt->close();
				return false;
			}
		}

		// request presensi user
		public function  getPresensiUser($idKaryawan,$tanggal){

			$stmt= $this->conn->prepare("SELECT * FROM presensi WHERE tanggal='$tanggal' AND id_karyawan={$idKaryawan}");
			$stmt->execute();			
			$presensi=$stmt->get_result()->fetch_assoc();
		
			$stmt->close();

			return $presensi;
		}

		// request presensi user per bulan
		public function  PresensiBulanan($idKaryawan,$bulan,$tahun){

			$stmt= $this->conn->prepare("SELECT * FROM presensi WHERE MONTH(tanggal)='$bulan' AND YEAR(tanggal)= '$tahun' AND id_karyawan={$idKaryawan} ORDER BY tanggal ASC");
			$stmt->execute();			
			$presensi=$stmt->get_result();
		
			$stmt->close();

			return $presensi;
		}

		// request izin user per bulan
		public function  izinBulanan($idKaryawan,$bulan,$tahun){

			$stmt= $this->conn->prepare("SELECT * FROM izin WHERE MONTH(tanggal)='$bulan' AND YEAR(tanggal)= '$tahun' AND id_karyawan={$idKaryawan} AND status_1=1 ORDER BY tanggal ASC");
			$stmt->execute();			
			$data=$stmt->get_result();
		
			$stmt->close();

			return $data;
		}
		
		//fungsi untuk menghitung jumlah jam kerja kedalam database
		public function hitungJumlahJam($idKaryawan,$tanggal)
		{	
			$p=$this->getPresensiUser($idKaryawan,$tanggal);

			$kosong="0000-00-00 00:00:00";
			$break="-01:00:00";

			if ($p['koreksi_jp']!=$kosong && $p['koreksi_jd']!=$kosong) {
				$stmt=$this->conn->prepare("UPDATE presensi SET jumlah_jam= ADDTIME(TIMEDIFF(koreksi_jp,koreksi_jd),'$break') WHERE id_karyawan='$idKaryawan' AND tanggal='$tanggal' ");
			} elseif ($p['koreksi_jp']!=$kosong && $p['jam_datang']!=$kosong) {
				$stmt=$this->conn->prepare("UPDATE presensi SET jumlah_jam= ADDTIME(TIMEDIFF(koreksi_jp,jam_datang),'$break') WHERE id_karyawan='$idKaryawan' AND tanggal='$tanggal' ");
			} elseif ($p['koreksi_jd']!=$kosong && $p['jam_pulang']!=$kosong) {
				$stmt=$this->conn->prepare("UPDATE presensi SET jumlah_jam= ADDTIME(TIMEDIFF(jam_pulang,koreksi_jd),'$break') WHERE id_karyawan='$idKaryawan' AND tanggal='$tanggal' ");
			} elseif ($p['jam_pulang']!=$kosong && $p['jam_datang']!=$kosong) {
				$stmt=$this->conn->prepare("UPDATE presensi SET jumlah_jam= ADDTIME(TIMEDIFF(jam_pulang,jam_datang),'$break') WHERE id_karyawan='$idKaryawan' AND tanggal='$tanggal' ");
			} else {
				return false;
			}

			if($stmt->execute()){
				$stmt->close();
				return true;
			} else {
				$stmt->close();
				return false;
			}
		}

		//Hitung jam kerja yang kurang atau lebih
		public function hitungKeterangan($idKaryawan,$tanggal){
			$p=$this->getPresensiUser($idKaryawan,$tanggal);
			$kosong="00:00:00";
			$jam_kerja="08:00:00";

			if ($p['jumlah_jam'] != $kosong) {
				$stmt=$this->conn->prepare("UPDATE presensi SET keterangan=TIMEDIFF(jumlah_jam,'$jam_kerja') WHERE id_karyawan='$idKaryawan' AND tanggal='$tanggal' ");
			} else {
				return false;
			}

			if($stmt->execute()){
				$stmt->close();
				return true;
			} else {
				$stmt->close();
				return false;
			}
		}

		//lihat laporan presensi harian
		public function laporanHarian($tanggal)
		{
			$stmt = $this->conn->prepare("SELECT karyawan.nama, TIME(presensi.jam_datang), TIME(presensi.jam_pulang), TIME(presensi.koreksi_jd), TIME(presensi.koreksi_jp), presensi.shift FROM karyawan INNER JOIN presensi ON karyawan.id_karyawan=presensi.id_karyawan AND presensi.tanggal='$tanggal' ORDER BY karyawan.nama ASC ");

			if($stmt->execute()){
				$data=$stmt->get_result();
				$stmt->close();
				return $data;
			} else{
				return NULL;
			}
		}

		//menampilkan bulan dan tahun yang tersedia dalam tabel
		public function getBulanTahun(){
			$stmt = $this->conn->prepare("SELECT DISTINCT MONTH(tanggal) AS 'bulan', YEAR(tanggal) AS 'tahun' FROM presensi");

			if($stmt->execute()){
				$data=$stmt->get_result();
				$stmt->close();
				return $data;
			} else{
				return NULL;
			}
		}

		//Get QRcode content for today
		public function getQrcode(){
			$stmt = $this->conn->prepare("SELECT * FROM qrcode WHERE tanggal=CURDATE()");

			if($stmt->execute()){
				$qrcode=$stmt->get_result()->fetch_assoc();
				$stmt->close();
				return $qrcode;
			} else{
				return NULL;
			}
		}

		//cek keberadaan qrcode
		public function qrcodeExist($tanggal){
			$stmt = $this->conn->prepare("SELECT * FROM qrcode WHERE tanggal='$tanggal'");
			$stmt->execute();
			$stmt->store_result();

			if($stmt->num_rows == 1){
				//user exist
				$stmt->close();
				return true;
			} else {
				//user not exist
				$stmt->close();
				return false;
			}
		}

		//Store Qrcode ke database
		public function storeQrcode($tanggal,$qrcode){

			if ($this->qrcodeExist($tanggal)) {
				$stmt = $this->conn->prepare("UPDATE qrcode SET kode='$qrcode' WHERE tanggal='$tanggal'");				
			} else {
				$stmt = $this->conn->prepare("INSERT INTO qrcode(tanggal,kode) VALUES('$tanggal','$qrcode')");
			}
			
			$result = $stmt->execute();
			$stmt->close();

			if($result){
				return true;
			} else {
				return false;
			}
		}

		//dashboard karyawan 
		public function dashboardDatang(){
			$stmt = $this->conn->prepare("SELECT karyawan.nama,karyawan.mac_address, presensi.jam_datang FROM karyawan INNER JOIN presensi ON karyawan.id_karyawan=presensi.id_karyawan AND DATE(presensi.jam_datang)=CURRENT_DATE ORDER BY presensi.jam_datang DESC LIMIT 5");

			if($stmt->execute()){
				$data=$stmt->get_result();
				$stmt->close();
				return $data;
			} else{
				return NULL;
			}
		}

		public function dashboardPulang(){
			$stmt = $this->conn->prepare("SELECT karyawan.nama,karyawan.mac_address, presensi.jam_pulang FROM karyawan INNER JOIN presensi ON karyawan.id_karyawan=presensi.id_karyawan AND DATE(presensi.jam_pulang)=CURRENT_DATE ORDER BY presensi.jam_pulang DESC LIMIT 5");

			if($stmt->execute()){
				$data=$stmt->get_result();
				$stmt->close();
				return $data;
			} else{
				return NULL;
			}
		}

		/**
	     * Encrypting password
	     * @param password
	     * returns salt and encrypted password
	     */
	    public function hashSSHA($password) {
	 
	        $salt = sha1(rand());
	        $salt = substr($salt, 0, 10);
	        $encrypted = base64_encode(sha1($password . $salt, true) . $salt);
	        $hash = array("salt" => $salt, "encrypted" => $encrypted);
	        return $hash;
	    }
	 
	    /**
	     * Decrypting password
	     * @param salt, password
	     * returns hash string
	     */
	    public function checkHashSSHA($salt, $password) {
	        $hash = base64_encode(sha1($password . $salt, true) . $salt);
	        return $hash;
	    }

	    /**
	    * Fullcalender setup - dengan tabel izin & karyawan
	    */
	    public function agendaBaru($event,$id_karyawan,$tanggal,$jam_pergi,$jam_kembali,$keperluan){

			switch ($event) {
				case 'izin' :
					$stmt = $this->conn->prepare("INSERT INTO izin (`id_karyawan`, `tanggal`, `jam_pergi`, `jam_kembali`, `allday`, `keperluan`, `status_1`, `warna`, `jumlah_jam`) VALUES ('$id_karyawan','$tanggal','$jam_pergi','$jam_kembali',0,'$keperluan',1,'#00c0ef',TIMEDIFF('$jam_kembali','$jam_pergi'))");
					break;
				case 'cuti':
					$stmt = $this->conn->prepare("INSERT INTO izin (`id_karyawan`, `tanggal`, `jam_pergi`, `jam_kembali`, `allday`, `keperluan`, `status_1`, `warna`, `jumlah_jam`) VALUES ('$id_karyawan','$tanggal','$jam_pergi','$jam_kembali',1,'$keperluan',1,'#f012be','08:00:00')");
					$this->minusCuti($id_karyawan);
					break;
				case 'sakit':
					$stmt = $this->conn->prepare("INSERT INTO izin (`id_karyawan`, `tanggal`, `jam_pergi`, `jam_kembali`, `allday`, `keperluan`, `status_1`, `warna`, `jumlah_jam`) VALUES ('$id_karyawan','$tanggal','$jam_pergi','$jam_kembali',1,'$keperluan',1,'#00a65a','08:00:00')");
					break;
				default:
					$stmt = $this->conn->prepare("INSERT INTO izin (`id_karyawan`, `tanggal`, `jam_pergi`, `jam_kembali`, `allday`, `keperluan`, `status_1`, `warna`, `jumlah_jam`) VALUES ('$id_karyawan','$tanggal','$jam_pergi','$jam_kembali',1,'absen atau tidak ada keterangan',1,'#dd4b39','08:00:00')");
			}
			
			$result = $stmt->execute();
			$stmt->close();

			if($result){
				return true;
			} else {
				return false;
			}
	    }

	    public function permintaanIzin()
	    {
	    		
    		$stmt = $this->conn->prepare("SELECT * FROM izin WHERE status_1=0");
			$stmt->execute();
    		$data=$stmt->get_result();
    		$stmt->close();
			return $data;
	    }

	    public function setuju($id_izin){
	    	$stmt = $this->conn->prepare("UPDATE izin SET status_1=1 WHERE id_izin='$id_izin'");
	    	$stmt->execute();
    		$stmt->close();
			return true;	
	    }

	    public function hapus($id_izin){
			$stmt = $this->conn->prepare("DELETE FROM izin WHERE id_izin='$id_izin'");
	    	$cek=$this->cekIzin($id_izin,4);
    			if ($cek['cek']=='cuti') {
    				$this->plusCuti($cek['id']); }
	    	$stmt->execute();
    		$stmt->close();
			return true;
	    }

	    public function tampilAgenda(){
	    	$stmt = $this->conn->prepare("SELECT karyawan.nama, izin.id_izin, izin.jam_pergi, izin.jam_kembali, izin.keperluan, izin.allday, izin.warna FROM karyawan INNER JOIN izin ON karyawan.id_karyawan=izin.id_karyawan WHERE status_1=true");

			if($stmt->execute()){
				$data=$stmt->get_result();
				$stmt->close();
				return $data;
			} else{
				return NULL;
			}
	    }

	    /**
	    * fungsi baru :
	    * 1. set shift pada tabel presensi
	    * 2. set kalkulasi jatah cuti pada tabel karyawan
	    */

	    private function setShift($idKaryawan){
	    	$kar=$this->getUser($idKaryawan); $shift=$kar['shift'];
	    	$stmt = $this->conn->prepare("UPDATE presensi SET shift='$shift' WHERE id_karyawan='$idKaryawan' AND tanggal=CURDATE()");
	    	$stmt->execute();
    		$stmt->close();
			return true;	
	    }

	    public function cekIzin($id_izin,$length){
	    	$stmt = $this->conn->prepare("SELECT * FROM izin WHERE id_izin='$id_izin' ");
	    	$stmt->execute();
	    	$cek=$stmt->get_result()->fetch_assoc();
    		$isi= array('cek' =>substr($cek['keperluan'],0,$length) , 'id'=>$cek['id_karyawan'] ); 
			$stmt->close();
			return $isi;	
	    }

	    public function minusCuti($idKaryawan){
	    	$stmt = $this->conn->prepare("UPDATE karyawan SET jatah_cuti=jatah_cuti-1 WHERE id_karyawan='$idKaryawan' ");
	    	$stmt->execute();
    		$stmt->close();
			return true;	
	    }

	    public function plusCuti($idKaryawan){
	    	$stmt = $this->conn->prepare("UPDATE karyawan SET jatah_cuti=jatah_cuti+1 WHERE id_karyawan='$idKaryawan' ");
	    	$stmt->execute();
    		$stmt->close();
			return true;	
	    }

	    //fungsi non-SQL
	    //return nama bulan
		public function namaBulan($bulan){
			$mons = array(1 => "Januari", 2 => "Februari", 3 => "Maret", 4 => "April", 5 => "Mei", 6 => "Juni", 7 => "Juli", 8 => "Agustus", 9 => "September", 10 => "Oktober", 11 => "November", 12 => "Desember");
			$month_name = $mons[$bulan];
			return $month_name;
		}

		//fungsi cek jam kerja
		public function cekDatang($jam,$shift){
		    $j=(int)substr($jam,0,2);
		    $m=(int)substr($jam,3,2);

		  if ($shift==0 || $shift==1) {
		    if ($j>=8 && $m>=5) {
		       echo "<span class=\"badge bg-red\">".$jam."</span>";
		      } else{
		       echo "<span class=\"badge bg-green\">".$jam."</span>";
		      } 
		  } else {
		    if ($j>=13 && $m>=5) {
		       echo "<span class=\"badge bg-red\">".$jam."</span>";
		      } else{
		       echo "<span class=\"badge bg-green\">".$jam."</span>";
		      } 
		  }
		    
		} //.cekDatang

		public function cekPulang($jam,$shift){
		    $j=(int)substr($jam,0,2);
		    $m=(int)substr($jam,3,2);

		  if ($shift==0) {
		    if ($j<17) {
		       echo "<span class=\"badge bg-red\">".$jam."</span>";
		      } else{
		       echo "<span class=\"badge bg-green\">".$jam."</span>";
		      } 
		  } elseif ($shift==1) {
		    if ($j<16) {
		       echo "<span class=\"badge bg-red\">".$jam."</span>";
		      } else{
		       echo "<span class=\"badge bg-green\">".$jam."</span>";
		      } 
		  } else {
		    if ($j<21) {
		       echo "<span class=\"badge bg-red\">".$jam."</span>";
		      } else{
		       echo "<span class=\"badge bg-green\">".$jam."</span>";
		      }
		  }
		    
		} //.cekPulang

}
 ?>