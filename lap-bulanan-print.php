<?php 
session_start();
error_reporting(0);

require_once 'include/db_function.php';
$db= new db_function();

if (isset($_SESSION['admin'])||isset($_SESSION['tamu'])) {
 ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>GePresence | Invoice</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="template/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="template/dist/css/AdminLTE.min.css">

  </head>
  <body onload="window.print();">
<div class="wrapper">
      <!-- Main content -->
<section class="invoice">
  <!-- title row -->
  <div class="row invoice-info">
    <div class="col-md-10 invoice-col">
      <h2 class="page-header">
        <i class="fa fa-globe"></i> PT. Geschool Cerdas Mandiri
      </h2>
    </div><!-- /.col -->
  <div class="col-md-2 invoice-col"></div>
  </div> <!-- .row-->
  <!-- info row -->
  <div class="row invoice-info">
    <div class="col-sm-5 invoice-col">
      From :
      <address>
        <strong>Geschool Admin</strong><br>
        Jl. Timoho no.67, Gondokusuman<br>
        Kota Yogyakarta<br>
        Phone : (0274) 633181<br>
        Email : admin@geschool.net
      </address>
    </div><!-- /.col -->
    <div class="col-sm-5 invoice-col">
      To :
<?php 
    $data=$db->getUser($_REQUEST['id']);
 ?>

      <address>
        <strong><?php echo ucwords($data['nama']); ?></strong><br>
        Phone : <?php echo $data['telepon']; ?><br>
        Email : <?php echo $data['email']; ?>
      </address>
    </div><!-- /.col -->
    <div class="col-sm-2 invoice-col">
      <b>Date : </b><?php echo date('d-m-Y'); ?><br>
    </div><!-- /.col -->
  </div><!-- /.row -->

  <!-- Table row -->
  <div class="row">
    <div class="col-xs-12 table-responsive" id="tabel-bulanan">
      <p class="lead">Presensi - <?php echo $db->namaBulan($_REQUEST['bulan'])." ".$_REQUEST['tahun']; ?></p>
      <table class="table table-striped">
        <thead>
          <tr>
            <th>Tanggal</th>
            <th>Datang</th>
            <th>Pulang</th>
            <th>Koreksi D</th>
            <th>Koreksi P</th>
            <th>Durasi Kerja</th>
          </tr>
        </thead>
        <tbody>
  <?php 
  $pb=$db->presensiBulanan($_REQUEST['id'],$_REQUEST['bulan'],$_REQUEST['tahun']);

  $i=1; $telat=0; $tjk=0; $tmo=0;
  foreach ($pb as $value) {

    $hh=(int)substr($value['jam_datang'],11,2);
    $mm=(int)substr($value['jam_datang'],14,2);
    $hk=(int)substr($value['koreksi_jd'],11,2);
    $mk=(int)substr($value['koreksi_jd'],14,2);
    
    $jk=(int)substr($value['jumlah_jam'],0,2);
    $mo=(int)substr($value['jumlah_jam'],3,2);

    //Hitung jumlah terlambat kerja
    if($value['shift']==0 || $value['shift']==1){
      if ($hk!=00){
        if ($hk>=8 && $mk>=5) { $telat++; }
      } else { 
        if ($hh>=8 && $mm>=5) { $telat++; }
      }  
    } else {
      if ($hk!=00){
        if ($hk>=13 && $mk>=5) { $telat++; }
      } else { 
        if ($hh>=13 && $mm>=5) { $telat++; }
      }
    }

    $tjk=$tjk+$jk;//hitung jumlah jam kerja
    $tmo=$tmo+$mo;//hitung jumlah menit kerja
   ?>
          <tr>
            <td><?php echo $value['tanggal']; ?></td>
            <td><?php echo substr($value['jam_datang'],11); ?></td>
            <td><?php echo substr($value['jam_pulang'],11); ?></td>
            <td><?php echo substr($value['koreksi_jd'],11); ?></td>
            <td><?php echo substr($value['koreksi_jp'],11); ?></td>
            <td><?php echo $value['jumlah_jam']; ?></td>
          </tr>
<?php $i++; } 
  $tmo_j=floor($tmo / 60);
  
  if ($tmo>=60) {
      $tmo_m=$tmo % 60;
  } else { $tmo_m=$tmo;}

?>
        </tbody>
      </table>
    </div><!-- /.col -->
  </div><!-- /.row -->

  <div class="row">
    <!-- izin & cuti column -->
    <div class="col-xs-9">
      <p class="lead">Izin atau Cuti</p>
        <div class="table-responsive">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>Tanggal</th>
            <th>Pergi</th>
            <th>Kembali</th>
            <th>Durasi</th>
          </tr>
        </thead>
        <tbody>
  <?php 
  $iz=$db->izinBulanan($_REQUEST['id'],$_REQUEST['bulan'],$_REQUEST['tahun']);

  $a=1; $tji=0; $tmi=0;
  foreach ($iz as $value) {
    $ji=(int)substr($value['jumlah_jam'],0,2);
    $mi=(int)substr($value['jumlah_jam'],3,2);
    
    $tji=$tji+$ji; //hitung total jam izin
    $tmi=$tmi+$mi; //hitung total menit izin
   ?>
          <tr>
            <td><?php echo $value['tanggal']; ?></td>
            <td><?php echo $value['jam_pergi']; ?></td>
            <td><?php echo $value['jam_kembali']; ?></td>
            <td><?php echo $value['jumlah_jam']; ?></td>
          </tr>
<?php $a++; } 

  $tmi_j=floor($tmi / 60);
  
  if ($tmi>=60) {
      $tmi_m=$tmi % 60;
  } else { $tmi_m=$tmi;}
?>
        </tbody>
      </table>
    </div><!-- /.table-responsive -->

    </div><!-- /.col -->
    <div class="col-xs-3">
      <p class="lead">Keterangan</p>
      <div class="table-responsive">
        <table class="table">
          <tr>
            <th style="width:50%">Terlambat</th>
            <td>: <?php echo $telat; ?></td>
          </tr>
          <tr>
            <th>Entri Presensi</th>
            <td>: <?php echo ($i-1)." Hari"; ?></td>
          </tr>
          <tr>
            <th>Tot. Jam Kerja</th>
            <td>: <?php echo ($tjk+$tmo_j)." j : ".$tmo_m." m"; ?></td>
          </tr>
          <tr>
            <th>Entri Izin</th>
            <td>: <?php echo ($a-1)." Kali"; ?></td>
          </tr>
          <tr>
            <th>Tot. Jam Izin</th>
            <td>: <?php echo ($tji+$tmi_j)." j : ".$tmi_m." m"; ?></td>
          </tr>
        </table>
      </div>
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>
    </div><!-- ./wrapper -->

    <!-- AdminLTE App -->
    <script src="template/dist/js/app.min.js"></script>
  </body>
</html>
<?php } ?>
