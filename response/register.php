<?php 
	require_once '../include/db_function.php';
	$db= new db_function();

	//json respon array
	$response = array("error" => FALSE);

	if (isset($_POST['nama'])&&isset($_POST['email'])&&isset($_POST['password'])&&isset($_POST['telepon'])&&isset($_POST['mac'])){

		$nama		=ucwords($_POST['nama']);
		$email		=$_POST['email'];
		$password	=$_POST['password'];
		$telepon	=$_POST['telepon'];
		$mac		=$_POST['mac'];

		//check if user is already exist
		if ($db->isUserExist($email,$mac,$telepon)) {
			$response["error"]= TRUE;
			$response["error_msg"]="Data karyawan digunakan oleh ".$email;
			echo json_encode($response);
		} else{
			//create new user
			$user = $db->storeUser($nama,$email,$password,$telepon,$mac);
			if ($user) {
				$response["error"]=FALSE;
				$response["user"]["id_karyawan"]=$user["id_karyawan"];
				$response["user"]["nama"]=$user["nama"];
				$response["user"]["email"]=$user["email"];
				$response["user"]["telepon"]=$user["telepon"];
				$response["user"]["mac"]=$user["mac_address"];
				$response["user"]["created_at"]=$user["created_at"];
				echo json_encode($response);
			} else {
				//user failed to store
				$response["error"] = TRUE;
				$response["error_msg"]="Maaf, terjadi kesalahan saat registrasi!";
				echo json_encode($response);
			}
		}
	} else{
		$response["error"]=TRUE;
		$response["error_msg"]="Data yang dibutuhkan tidak lengkap!";
		echo json_encode($response);
	}
 ?>