<?php 
	require_once '../include/db_function.php';
	$db= new db_function();

	//json response array
	$response= array("error"=>FALSE);

	if(isset($_POST['email'])&&isset($_POST['password'])&&isset($_POST['mac'])){
		//receiving the post params
		$email = $_POST['email'];
		$password= $_POST['password'];
		$mac= $_POST['mac'];

		//get the user by email and password
		$user=$db->getUserData($email,$password,$mac);

		if ($user != false) {
			//user is found
			$response["error"]=FALSE;
				$response["user"]["id_karyawan"]=$user["id_karyawan"];
				$response["user"]["nama"]=$user["nama"];
				$response["user"]["email"]=$user["email"];
				$response["user"]["telepon"]=$user["telepon"];
				$response["user"]["mac"]=$user["mac_address"];
				$response["user"]["created_at"]=$user["created_at"];
				echo json_encode($response);
		}else{
			$response["error"] = TRUE;
				$response["error_msg"]="Data tidak cocok, login gagal!";
				echo json_encode($response);
		}
	} else {
		$response["error"] = TRUE;
				$response["error_msg"]="Data tidak lengkap atau HP tidak terdaftar!";
				echo json_encode($response);
	}

 ?>