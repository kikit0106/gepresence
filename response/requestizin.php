<?php
	require_once '../include/db_function.php';
	$db= new db_function();

	//json response array
	$response= array("error"=>FALSE);

	if (isset($_POST['pergi'])&&isset($_POST['kembali'])&&isset($_POST['keperluan'])&&isset($_POST['id_karyawan'])) {

		//receiving the post params
		$id_karyawan=$_POST['id_karyawan'];
		$pergi=$_POST['pergi'];
		$kembali=$_POST['kembali'];
		$keperluan="izin - ".$_POST['keperluan'];
		
		//create new izin request 
		$user = $db->storeIzin($id_karyawan,$pergi,$kembali,$keperluan);
			if ($user) {
				$response["error"]=FALSE;
				echo json_encode($response);
			} else {
				//user failed to store
				$response["error"] = TRUE;
				$response["error_msg"]="Maaf, terjadi kesalahan saat permintaan izin ke server!";
				echo json_encode($response);
			}
	
	} else{
		$response["error"]=TRUE;
		$response["error_msg"]="Data yang dibutuhkan tidak lengkap!";
		echo json_encode($response);
	}
?>