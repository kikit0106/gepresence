<?php
	require_once '../include/db_function.php';
	$db= new db_function();

	//json response array
	$response= array("error"=>FALSE);

	if (isset($_POST['result'])&&isset($_POST['condition'])&&isset($_POST['id_karyawan'])) {

		//receiving the post params
		$id_karyawan=$_POST['id_karyawan'];
		$scanResult=$_POST['result'];
		$kondisi=$_POST['condition'];
		
		//get QRcode content for today
		$qrcode=$db->getQrcode();

		if ($scanResult == $qrcode["kode"]) {

			
			if ($kondisi=="datang") {
				# code...
				$presensi = $db->prepareJamDatang($id_karyawan,"0000-00-00 00:00:00");
				
				if ($presensi) {
					$response["error"]=FALSE;
					$response["condition"]=$kondisi;
					echo json_encode($response);
				} else {
					$response["error"]=TRUE;
					$response["error_msg"]="Query jam datang tidak berhasil!";
					echo json_encode($response);
				}

			} elseif ($kondisi=="pulang") {
				# code...
				$presensi = $db->prepareJamPulang($id_karyawan,"0000-00-00 00:00:00");
				
				if ($presensi) {
					$response["error"]=FALSE;
					$response["condition"]=$kondisi;
					echo json_encode($response);
				} else {
					$response["error"]=TRUE;
					$response["error_msg"]="Query jam pulang tidak berhasil!";
					echo json_encode($response);
				}

			} else {
				$response["error"]=TRUE;
				$response["error_msg"]="QRCode cocok, tetapi kondisi tidak ditemukan!";
				echo json_encode($response);
			}
				echo json_encode($response);
		} else {
			$response["error"]=TRUE;
			$response["error_msg"]="QRcode result not found!";
				echo json_encode($response);
		}

	} else {
		$response["error"]=TRUE;
		$response["error_msg"]="Data tidak ditemukan!";
			echo json_encode($response);
	}
?>