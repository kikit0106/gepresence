<?php 
error_reporting(0);

require_once 'include/db_function.php';
$db= new db_function();

$data=$db->laporanHarian($_REQUEST['tanggal']);
      
     if (!empty($data)) {
     	?> 
				  <table id="data-p" class="table table-bordered table-striped table-hover">
                    <thead>
                      <tr>
                        <th style="width: 10px;">No</th>
                        <th style="width: 175px;">Nama</th>
                        <th>Datang</th>
                        <th>Pulang</th>
                        <th>Koreksi D</th>
                        <th>Koreksi P</th>
                      </tr>
                    </thead>
                    <tbody>
     	<?php
            $i=1;
         	foreach ($data as $value) {
					 ?>

					 <tr>
              <td><?php echo $i; ?></td>
              <td ><?php echo ucwords($value['nama']); ?></td>
              <td >
              <?php if($value['TIME(presensi.jam_datang)']!="00:00:00") {
                $db->cekDatang($value['TIME(presensi.jam_datang)'],$value['shift']);
                } ?>
              </td>
              <td>
              <?php if($value['TIME(presensi.jam_pulang)']!="00:00:00") {
                $db->cekPulang($value['TIME(presensi.jam_pulang)'],$value['shift']);
                } ?>
              </td>
              <td>
              <?php if($value['TIME(presensi.koreksi_jd)']!="00:00:00") {
                  echo "<span class=\"badge bg-blue\">".$value['TIME(presensi.koreksi_jd)']."</span>";
                } ?>
              </td>
              <td>
              <?php if($value['TIME(presensi.koreksi_jp)']!="00:00:00") {
                  echo "<span class=\"badge bg-blue\">".$value['TIME(presensi.koreksi_jp)']."</span>";
                } ?>
              </td>
           </tr>
         <?php $i++; } ?>
		         	</tbody>
                </table>
    <script type="text/javascript">
    	$(function () {
          $("#data-p").DataTable({
            "paging": false,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true
          	});
	       });
    </script>
        <?php	 }  ?>
                    