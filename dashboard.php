<?php
session_start();
error_reporting(0);

require_once 'include/db_function.php';
$db= new db_function();    
    //set timezone
    $timezone = "Asia/Jakarta";
    date_default_timezone_set($timezone);
    
    $qrcode=$db->getQrcode();

if (isset($_SESSION['admin'])||isset($_SESSION['tamu'])) {
?>
<!-- col-md -->
<div class="col-md-6">
    <div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Scan QRCode dibawah ini saat presensi</h3>
        <div class="box-tools pull-right">
            <button title="Collapse" class="btn btn-box-tool" data-toggle="tooltip" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div><!-- /.box-header-->
    <div class="box-body">
       <div id="qrcodeCanvas" ></div>
            <img id="qrcode" style="display: block; margin-left: auto;margin-right: auto" src="https://zxing.org/w/chart?cht=qr&chs=335x335&chld=L&choe=UTF-8&chl=<?php echo $qrcode['kode']; ?>" />
    </div><!-- /.box-body -->  
    </div><!--/.box -->
</div><!-- /.col-md -->
<div class="col-md-6">
    <div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Informasi</h3>
        <div class="box-tools pull-right">
            <button title="Collapse" class="btn btn-box-tool" data-toggle="tooltip" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div><!-- /.box-header-->
    <div class="box-body">
         <div class="small-box bg-blue">
            <div class="inner">
             <p>Jam</p>
              <h3 id="clock"><?php echo date('H:i:s'); ?></h3>
            </div>
            <div class="icon">
              <i class="fa fa-clock-o"></i>
            </div>
        </div>
        <div class="box box-success">
                <div class="box-header">
                  <h3 class="box-title">DATANG</h3>
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                    <div class="datang-putar">

                    </div> <!--jquery auto reload-->
                </div><!-- /.box-body -->
        </div><!--  /-box-info  -->

        <div class="box box-warning">
                <div class="box-header">
                  <h3 class="box-title">PULANG</h3>
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                    <div class="pulang-putar">     
               
                    </div> <!--jquery auto reload-->
                </div><!-- /.box-body -->
        </div><!--  /-box-info  -->

    </div><!-- /.box-body -->
    </div><!--/.box -->
</div><!-- /.col-md -->
<?php
} else {
    header("location:index.php");
 }
?>