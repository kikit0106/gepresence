<?php 
error_reporting(0);
session_start();

require_once 'include/db_function.php';
$db= new db_function();

if (isset($_SESSION['admin'])) { 
  $data=$db->getAllUser();
  ?>
<div class="col-md-3">
	<div class="box box-info">
    <div class="box-header with-border">
        <h4 class="box-title">Agenda</h4><small> "Drag & drop"</small>
        <div class="box-tools pull-right">
            <button title="Collapse" class="btn btn-box-tool" data-toggle="tooltip" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
      </div>
      <div class="box-body">
        
      <div class="btn-group" style="width: 100%; margin-bottom: 10px;">
        <!--<button type="button" id="color-chooser-btn" class="btn btn-info btn-block dropdown-toggle" data-toggle="dropdown">Color <span class="caret"></span></button>-->
        <ul class="fc-color-picker" id="color-chooser">
          <li><a title="Izin" data-toggle="tooltip" class="text-aqua" href="Izin"><i class="fa fa-square"></i> I </a></li>
          <li><a title="Cuti" data-toggle="tooltip" class="text-fuchsia" href="Cuti"><i class="fa fa-square"></i> C </a></li>
          <li><a title="Sakit" data-toggle="tooltip" class="text-green" href="Sakit"><i class="fa fa-square"></i> S </a></li>
          <li><a title="Absen" data-toggle="tooltip" class="text-red" href="Absen"><i class="fa fa-square"></i> A </a></li>
        </ul>
      </div><!-- /btn-group -->

    <form>
      <div class="form-group" id="form-agenda">
        <!-- <input id="new-event" type="text" class="form-control" placeholder="Event Title"> -->
        <div class="form-group has-feedback">
            <label>Nama Karyawan :</label>
            <select id="karyawan" class="form-control" style="width: 100%;">
        <?php 
          foreach ($data as $v) { ?>
            <option data-id="<?php echo $v['id_karyawan']; ?>"><?php echo ucwords($v['nama']); ?></option>
          <?php } ?>
            </select>                    
        </div>

        <div id="tgl" class="form-group has-feedback">
          <label>Tanggal :</label>
          <input id="tanggal" type="text" class="form-control"  value="<?php echo date('Y-m-d'); ?>" >  
          <span class="fa fa-calendar form-control-feedback"></span>
        </div>
        
        <div class="form-group has-feedback waktu bootstrap-timepicker">
          <label>Pergi :</label>
          <input id="pergi" type="text" class="form-control time">
          <span class="fa fa-clock-o form-control-feedback"></span>
        </div>

        <div class="form-group has-feedback waktu bootstrap-timepicker">
          <label>Kembali :</label>
          <input id="kembali" type="text" class="form-control time">
          <span class="fa fa-clock-o form-control-feedback"></span>
        </div>

        <div id="pl" class="form-group has-feedback">
          <label>Keterangan :</label>
          <input id="perlu" type="text" class="form-control" placeholder="Keterangan..">
          <span class="fa fa-list-alt form-control-feedback"></span>
        </div>

        <button id="add-new-event" type="button" class="btn btn-info btn-flat pull-right">Izin</button>

      </div><!-- /form-group -->
    </form>   
	
   </div><!-- /.box-body -->
 
   <div id="hapus-agenda" class="box-footer">
     <h4 style="width:100%;" class="btn bg-purple" ><i class="glyphicon glyphicon-trash text-white"></i>  Hapus Agenda </h4>
   </div>

</div><!-- /. box -->
          <div class="box box-primary">
	            <div class="box-header with-border">
	       <?php $r=$db->permintaanIzin(); $n=0; foreach ($r as $h) { $n++; } ?>
                <div class="box-title">Permintaan izin 
                </div>
                <small class="label label-info"><?php echo $n; ?></small>
                <div class="box-tools pull-right">
                    <button title="Collapse" class="btn btn-box-tool" data-toggle="tooltip" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
	            </div>
	            <div class="box-body" id="permintaan-izin">
                  <ul class="todo-list">
         <?php 
         foreach ($r as $value) {
          $nk=$db->getUser($value['id_karyawan']); ?>
                  <li title=" <?php echo $value['keperluan']; ?> " data-toggle="tooltip">
                    <span class=" text text-blue"><?php echo $nk['nama']; ?></span>
                    <small><?php echo " ( ".$value['tanggal']." ) "; ?></small><br>
                    <small class="text"><?php echo substr($value['jam_pergi'],11,8)." - ".substr($value['jam_kembali'],11,8); ?></small>
                    <!-- General tools such as edit or delete-->
                    <div class="tools">
                      <i class="fa fa-edit app" id="setuju" title="setuju" data-toggle="tooltip" data-placement="bottom" data-id="<?php echo $value['id_izin']; ?>"></i>
                      <i class="fa fa-trash-o app" id="hapus" title="hapus" data-toggle="tooltip" data-placement="bottom" data-id="<?php echo $value['id_izin']; ?>"></i>
                    </div>
                  </li>
         <?php 
          }
          ?>           
                  </ul>
	            </div><!-- .box-body-->
          </div><!-- .box  -->
</div><!-- .col-->
<div class="col-md-9">
	<div class="box box-info no-padding">
      <!-- THE CALENDAR -->
      <div id="calendar"></div>
    </div><!-- /.box-body -->
</div><!-- .col-->

<!-- Page specific script -->
<script>
    // Script for calender
  $(document).ready(function () {
        
        $("#karyawan").select2();
        $(".select2-hidden-accessible").hide();
        //sembunyikan form
        $("#form-agenda").hide();
        $(".waktu").hide();

        $('#tanggal').datepicker({
          format:"yyyy-mm-dd"
        });

        $('.time').timepicker({
          showInputs: false,
          showMeridian : false,
          showSeconds : true
        });

        //fungsi untuk deteksi perpindahan cursor (fitur hapus)
        var currentMousePos = {
            x: -1,
            y: -1
          };
        jQuery(document).on("mousemove", function (event) {
              currentMousePos.x = event.pageX;
              currentMousePos.y = event.pageY;
        });

        //cek posisi mouse apakah diatas <div>
        function isElemOverDiv() {
        var trashEl = jQuery('#hapus-agenda');

        var ofs = trashEl.offset();

        var x1 = ofs.left;
        var x2 = ofs.left + trashEl.outerWidth(true);
        var y1 = ofs.top;
        var y2 = ofs.top + trashEl.outerHeight(true);

        if (currentMousePos.x >= x1 && currentMousePos.x <= x2 &&
            currentMousePos.y >= y1 && currentMousePos.y <= y2) {
            return true;
        }
        return false;
        }

        /* initialize the calendar
         -----------------------------------------------------------------*/
        //Date for the calendar events (dummy data)
        var date = new Date();
        var d = date.getDate(), m = date.getMonth(), y = date.getFullYear();
        var hh = date.getHours(), mm = date.getMinutes(), ss = date.getSeconds();

        $('#calendar').fullCalendar({
          header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek'
          },
          buttonText: {
            today: 'Hari ini',
            month: 'Bulan',
            week: 'Minggu'
          },
          //Random default events
          events: [
            {
              title: 'Hari ini',
              start: new Date(y, m, d ),
              end: new Date(y, m, d ),
              allDay: true,
              backgroundColor: "#4a202b",
              borderColor: "#4a202b"
            }
            <?php 
              $t=$db->tampilAgenda();
              foreach ($t as $value) { ?>
           ,{
              id : '<?php echo $value['id_izin']; ?>',
              title: '<?php echo $value['nama']; ?>',
              start: '<?php echo $value['jam_pergi']; ?>',
              end: '<?php echo $value['jam_kembali']; ?>',
              description: '<?php echo $value['keperluan']; ?>',
              allDay: <?php echo (($value['allday']==1)?'true':'false'); ?>,
              backgroundColor: "<?php echo $value['warna']; ?>",
              borderColor: "<?php echo $value['warna']; ?>"
            }  
              <?php
              }
             ?>
          ],
          eventRender: function(event, element) {
            element.qtip({
                content: event.description,
                position:{my:'left bottom',at:'top center'}
                 });
          },
          editable: true,
          droppable: true,
          timeFormat: 'H(:mm)',
          lang:'id', // this allows things to be dropped onto the calendar !!!
          eventDragStop: function (event, jsEvent, ui, view) {
          if (isElemOverDiv()) {
            var con = confirm('Anda yakin untuk menghapus agenda ini?');
            if(con == true) {
                var h='hapus';
                aksiAgenda(h,event.id);
              }   
            }
          }
        });

        /* ADDING EVENTS */
        var currColor = "#00c0ef";
        var currText = "";
        //Color chooser button
        var colorChooser = $("#color-chooser-btn");

        $("#color-chooser > li > a").click(function (e) {
          e.preventDefault();
          //Save color
          currColor = $(this).css("color");
          currText = $(this).attr("href");

          switch(currText){
            case 'Izin'  : $("#form-agenda").fadeIn(); $(".waktu").fadeIn(); $("#pl").fadeIn(); break;
            case 'Cuti'  : $("#form-agenda").fadeIn(); $(".waktu").fadeOut(); $("#pl").fadeIn(); break;
            case 'Sakit' : $("#form-agenda").fadeIn(); $(".waktu").fadeOut(); $("#pl").fadeIn(); break;
            case 'Absen' : $("#form-agenda").fadeIn(); $(".waktu").fadeOut(); $("#pl").fadeOut(); break;
            default : $("#form-agenda").fadeOut();
          }
          //Add color effect to button
          $('#add-new-event').html(currText);
          $('#add-new-event').css({"background-color": currColor, "border-color": currColor});
        });

        $("#add-new-event").click(function (e) {
          e.preventDefault();
          //Get value and make sure it is not null
          var id = $("#karyawan option:selected").attr("data-id");
          var tgl= $("#tanggal").val();
          var jp= $("#pergi").val();
          var jk= $("#kembali").val();
          var pr= $("#perlu").val();
          var e= $(this).html().toLowerCase();
          if (id.length == 0) {
            return;
          }
          $.ajax({
          dataType: "json",
          type  : "POST",
          url   : "action/agenda-crud.php",
          data  : "tipe="+e+"&id="+id+"&tanggal="+tgl+"&pergi="+jp+"&kembali="+jk+"&perlu="+pr,
          success: function(response){
            if(response.status == 'success'){
              $('#calendar').fullCalendar('addEventSource', response.newevent);
              var tgl= $("#tanggal").val("<?php echo date('Y-m-d'); ?>");
              var jp= $("#pergi").val("<?php echo date('H:i:s'); ?>");
              var jk= $("#kembali").val("<?php echo date('H:i:s'); ?>");
              var pr= $("#perlu").val("");
              $("#form-agenda").fadeOut();
              }                          
          },
          error: function(e){             
            alert('Error processing your request: '+e.responseText);
          }
        });
      });
      
      //approve permintaan izin dari android
      $(".app").click(function(){
        var e=$(this).attr('id');
        var id=$(this).attr('data-id');
        
        aksiAgenda(e,id);
      });

      function aksiAgenda (e,id) {
        $.ajax({
            dataType: "json",
            type  : "POST",
            url   : "action/agenda-crud.php",
            data  : "tipe="+e+"&id="+id,
            success: function(response){
              if(response.status == 'success'){
                 load_page('if');
                //$('#calendar').fullCalendar('removeEvents');
              }            
            },
            error: function(e){             
              alert('Error processing your request: '+e.responseText);
            }
          });
      }

  });// end of $(document).ready()
</script>

<?php 
} else {
header("location:index.php");
} ?>