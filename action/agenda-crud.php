<?php 
error_reporting(0);

require_once '../include/db_function.php';
$db= new db_function();

$tipe=$_POST['tipe'];
	if (!empty($tipe)) {
	 	// inisialissasi
		$id=$_POST['id'];
		$tanggal=strip_tags($_POST['tanggal']);
		$p=strip_tags($_POST['pergi']);
		$pergi=$tanggal." ".$p;
		$k=strip_tags($_POST['kembali']);
		$kembali=$tanggal." ".$k;
		if (!empty($_POST['perlu'])) {
		$perlu=$_POST['perlu'];
		} else { $perlu = 'tidak ada keterangan'; }

		if ($tipe=='izin') {
			$perlu='izin - '.$perlu;
			$in=$db->agendaBaru($tipe,$id,$tanggal,$pergi,$kembali,$perlu);
			if($in){
				$hasil = array('status'=>'success');
				$karyawan = $db->getUser($id);
				$hasil['newevent'] = array(array('id'=>$id,'title'=>$karyawan['nama'],'start'=>$pergi,'end'=>$kembali,'description'=>$perlu,'allDay'=>false,'backgroundColor'=>'#00c0ef','borderColor'=>'#00c0ef'));
				echo json_encode($hasil);
			}
			else{
				echo json_encode(array('status'=>'failed'));
			}

		}

		if ($tipe=='cuti') {
			$perlu='cuti - '.$perlu;
			$in=$db->agendaBaru($tipe,$id,$tanggal,$pergi,$kembali,$perlu);
			if($in){
				$hasil = array('status'=>'success');
				$karyawan = $db->getUser($id);
				$hasil['newevent'] = array(array('id'=>$id,'title'=>$karyawan['nama'],'start'=>$pergi,'end'=>$kembali,'description'=>$perlu,'allDay'=>true,'backgroundColor'=>'#f012be','borderColor'=>'#f012be'));
				echo json_encode($hasil);
			}
			else{
				echo json_encode(array('status'=>'failed'));
			}

		}

		if ($tipe=='sakit') {
			$perlu='sakit - '.$perlu;
			$in=$db->agendaBaru($tipe,$id,$tanggal,$pergi,$kembali,$perlu);
			if($in){
				$hasil = array('status'=>'success');
				$karyawan = $db->getUser($id);
				$hasil['newevent'] = array(array('id'=>$id,'title'=>$karyawan['nama'],'start'=>$pergi,'end'=>$kembali,'description'=>$perlu,'allDay'=>true,'backgroundColor'=>'#00a65a','borderColor'=>'#00a65a'));
				echo json_encode($hasil);
			}
			else{
				echo json_encode(array('status'=>'failed'));
			}

		}

		if ($tipe=='absen') {
			$in=$db->agendaBaru($tipe,$id,$tanggal,$pergi,$kembali,$perlu);
			if($in){
				$hasil = array('status'=>'success');
				$karyawan = $db->getUser($id);
				$hasil['newevent'] = array(array('id'=>$id,'title'=>$karyawan['nama'],'start'=>$pergi,'end'=>$kembali,'description'=>$perlu,'allDay'=>true,'backgroundColor'=>'#dd4b39','borderColor'=>'#dd4b39'));
				echo json_encode($hasil);
			}
			else{
				echo json_encode(array('status'=>'failed'));
			}

		}
		
		if ($tipe=='setuju') {
			$in=$db->setuju($id);
			if($in){
				$hasil = array('status'=>'success');
				echo json_encode($hasil);
			}
			else{
				echo json_encode(array('status'=>'failed'));
			}
		}

		if ($tipe=='hapus') {
			$in=$db->hapus($id);
			if($in){
				$hasil = array('status'=>'success');
				echo json_encode($hasil);
			}
			else{
				echo json_encode(array('status'=>'failed'));
			}
		}

	 } // end if !=empty($tipe)
?>