<?php 
error_reporting(0); 
session_start(); 

if (isset($_SESSION['admin'])){
  header("location:home.php");
  } else { ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>GePresence | Masuk</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="template/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="template/dist/css/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="template/dist/css/AdminLTE.min.css">
    <!-- JQuery-Confirm -->
    <link rel="stylesheet" href="template/dist/css/jquery-confirm.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition login-page">
    <div class="login-box">
      <div class="login-logo">
        <b>Ge</b>Presence
      </div><!-- /.login-logo -->

      <div id= "box" class="box">

        <div class="login-box-body">

        <p class="login-box-msg">Masuk menggunakan akun GePresence</p>

        <div id="login-box" ></div>
        
        <form action="./" method="post" id="form-login">

         <div class="form-group has-feedback">
            <input id="akun_email" type="email" class="form-control" placeholder="Email">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div> 

          <div class="form-group has-feedback">
            <input id="akun_pass" type="password" class="form-control" placeholder="Password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>

          <div class="row">
            <div class="col-xs-8">

            </div><!-- /.col -->

            <div class="col-xs-4">
              <button id="login" class="btn btn-success btn-block btn-flat" type="submit">Masuk</button>
            </div><!-- /.col -->

          </div>

        </form>
          <a href="#" id="lupa">Lupa password?</a>
      </div>

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <!-- jQuery 2.1.4 -->
    <script src="template/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Ajax request cek login-->
    <script type="text/javascript" src="template/dist/js/login.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="template/bootstrap/js/bootstrap.min.js"></script>
    <!-- jQuery-Confirm -->
    <script src="template/dist/js/jquery-confirm.min.js"></script>
    <!-- script validasi dan alert lupa paswword-->
    <script type="text/javascript">
      $(document).ready(function()
      { 
        $('#lupa').on('click', function () {
              $.confirm({
                  theme: 'white',
                  title: 'Kamu lupa password?',
                  content: '<p style="text-align: justify;">Mohon segera menghubungi administrator untuk melakukan penggantian password baru pada menu Data karyawan > Edit .<br><br><b>Terimakasih</b></p>',
                  confirmButton: 'Oke',confirmButtonClass: 'btn-success',cancelButton: false
              });
          });
      });//.document
    </script>

  </body>
</html>
  <?php 
}
  ?>