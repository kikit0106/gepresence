<?php 
error_reporting(0);
session_start();

if (isset($_SESSION['admin'])||isset($_SESSION['tamu'])) { 
  ?>
<div id="ad">
  <div class="col-md-12">
  <div class="box box-success no-padding">
    <div class="box-body">
      <div id="web">
        <h2 style="text-align: center;">Halaman Administrator Gepresence</h2>

        <h3 style="text-align: justify;">Login</h3>

        <hr />
        <p style="text-align: justify;">Gunanakan email dan password yang sama dengan saat anda mendaftar melalui smartphone android.&nbsp;</p>

        <h3 style="text-align: justify;">Dashboard</h3>

        <hr />
        <p style="text-align: justify;">Tersedia tampilan Qrcode untuk di <em>scan</em> ketika karyawan melakukan presensi. Tampilan jam dan keterangan berada disebelah kanan Qrcode. Keterangan menampilkan 5 orang terakhir yang melakukan presensi pada hari tersebut.&nbsp;</p>

        <h3 style="text-align: justify;">Data karyawan</h3>

        <hr />
        <p style="text-align: justify;">Menampilkan daftar seluruh karyawan yang sudah melakukan pendaftaran&nbsp;melalui <em>smartphone&nbsp;</em>android atau tambah data melalui admin. Secara default semua data karyawan yang ada (berasal dari android atau tambah manual) berlaku&nbsp;shift kerja 0 (08.00-17.00) dan jatah cuti 0 hari,&nbsp;data dapat diperbaharui melalui menu edit oleh admin.</p>

        <p style="text-align: justify;"><strong>Tambah data karyawan:</strong></p>

        <ol>
          <li style="text-align: justify;">Tambahkan data karyawan,&nbsp;</li>
          <li style="text-align: justify;">Pilih tambah karyawan</li>
          <li style="text-align: justify;">Masukkan data karyawan yang ingin anda tambahkan, isi semua kolom.&nbsp;*<em>MAC address dapat di lihat pada halaman login aplikasi android gepresence</em>.</li>
          <li style="text-align: justify;">Pilih simpan. *&nbsp;<em>jika ada kesalahan, cek email, no.HP atau MAC address (tidak boleh terdupilkasi dengan data karyawan lain)</em></li>
        </ol>

        <p style="text-align: justify;"><strong>Edit :</strong></p>

        <p style="text-align: justify;">Gunakan menu edit untuk melihat detail data karyawan atau&nbsp;memperbaharui data karyawan.Menambahkan <em>permission</em> admin, shift kerja dan jatah cuti dapat pula dilakukan.&nbsp;</p>

        <p style="text-align: justify;"><strong>Saran:&nbsp;</strong><br />
        Jika karyawan berganti device, maka dapat dilakukan edit MAC address device baru pada menu edit. Untuk menjaga data riwayat presensi serta login pada aplikasi yg terinstall pada device baru.</p>

        <h3 style="text-align: justify;">Laporan presensi</h3>

        <hr />
        <p style="text-align: justify;">Menampilkan seluruh presensi karyawan per hari, melakukan koreksi presensi dengan klik tombol koreksi presensi dan melihat laporan presensi per bulan dengan klik tombol detail di samping nama karyawan.</p>

        <p style="text-align: justify;"><strong>Koreksi presensi :</strong></p>

        <ol>
          <li style="text-align: justify;">Pilih tanggal yg akan dikoreksi pada kolom lihat, lalu klik lihat.</li>
          <li style="text-align: justify;">Setelah tampil presensi pada yg yg dipilih, klik koreksi presensi.&nbsp;</li>
          <li style="text-align: justify;">Masukkan data pada kolom yg tesedia.</li>
          <li style="text-align: justify;">Pilih simpan.&nbsp;</li>
        </ol>

        <h3 style="text-align: justify;">Informasi</h3>

        <hr />
        <p style="text-align: justify;">Berisi kalender yang dapat digunakan untuk mengagendakan&nbsp;serta&nbsp;menampilkan aktivitas&nbsp;izin, &nbsp;cuti, sakit atau absen karyawan.</p>

        <ul>
          <li style="text-align: justify;">Tambah data dapat dilakukan dengan klik salah&nbsp;satu huruf I, C, S dan A sesuai kebutuhan, lengkapi keterangan sesuai kolom yang tersedia kemudian&nbsp;klik tombolnya.&nbsp;</li>
          <li style="text-align: justify;">Hapus data dapat dilakukan dengan menyeret (<em>click and drag</em>)&nbsp;nama pada kalender ke kotak hapus.</li>
        </ul>

        <hr />
        <p style="text-align: justify;"><strong>Perhatian!</strong>&nbsp;</p>

        <ul>
          <li style="text-align: justify;">Alamat email, nomer handphone dan MAC address (dari aplikasi android) bersifat&nbsp;unik, yang artinya setiap karyawan harus berbeda.&nbsp;</li>
          <li style="text-align: justify;">Menghapus data karyawan, juga menghapus riwayat&nbsp;presensi dari karyawan tersebut.&nbsp;</li>
        </ul>

        <p style="text-align: justify;">&nbsp;</p>

        <p style="text-align: justify;">Keterangan lebih lanjut, dapat menghubungi :<br />
        <strong>Pikiring Waskitha (Kikit)</strong>&nbsp;<br />
        <strong>0857-2525-7011</strong></p>
      </div><!--.web -->
    </div>
  </div><!-- /.box -->
 </div><!-- .col-->
</div>


<div id="tm">
  <div class="col-md-12">
  <div class="box box-success no-padding">
    <div class="box-body">
      <div>
        <h2 style="text-align: center;">Halaman Administrator Gepresence</h2>

        <h3 style="text-align: justify;">Login</h3>

        <hr />
        <p style="text-align: justify;">Gunanakan email dan password yang sama dengan saat anda mendaftar melalui smartphone android.&nbsp;</p>

        <h3 style="text-align: justify;">Dashboard</h3>

        <hr />
        <p style="text-align: justify;">Tersedia tampilan Qrcode untuk di <em>scan</em> ketika karyawan melakukan presensi. Tampilan jam dan keterangan berada disebelah kanan Qrcode. Keterangan menampilkan 5 orang terakhir yang melakukan presensi pada hari tersebut.&nbsp;</p>

        <h3 style="text-align: justify;">Laporan presensi</h3>

        <hr />
        <p style="text-align: justify;">Menampilkan seluruh presensi karyawan per hari dan melihat laporan presensi per bulan dengan klik tombol detail di samping nama karyawan.</p>
      </div>
    </div>
  </div><!-- /.box -->
 </div><!-- .col-->
</div>

<?php 
} else {
header("location:index.php");
} ?>