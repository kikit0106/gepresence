<?php 
error_reporting(0);
session_start();

if (isset($_SESSION['admin'])||isset($_SESSION['tamu'])) { ?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>GePresence | Beranda</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="template/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="template/dist/css/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="template/dist/css/AdminLTE.min.css">
    <!-- datepicker UI -->
    <link rel="stylesheet" href="template/dist/css/datepicker.css">
    <!-- qTip Tooltip for fullcalendar -->
    <link rel="stylesheet" href="template/dist/css/jquery.qtip.min.css">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="template/plugins/timepicker/bootstrap-timepicker.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="template/plugins/select2/select2.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="template/plugins/datatables/dataTables.bootstrap.css">
    <!-- fullCalendar 2.2.5-->
    <link rel="stylesheet" href="template/plugins/fullcalendar/fullcalendar.min.css">
    <link rel="stylesheet" href="template/plugins/fullcalendar/fullcalendar.print.css" media="print">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="template/dist/css/skins/_all-skins.min.css">
    <!-- JQuery-Confirm -->
    <link rel="stylesheet" href="template/dist/css/jquery-confirm.min.css">
</head>
<!-- ADD THE CLASS fixed TO GET A FIXED HEADER AND SIDEBAR LAYOUT -->
<!-- the fixed layout is not compatible with sidebar-mini -->
    <body class="fixed sidebar-mini skin-green-light">
    <!-- Site wrapper -->
    <div class="wrapper">

        <header class="main-header">
            <!-- Logo -->
            <div class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>Ge</b>P</span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"><b>Ge</b>Presence</span>
            </div>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a class="sidebar-toggle" role="button" href="#" data-toggle="offcanvas">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a id="keluar" class="link" aria-expanded="false"  data-toggle="dropdown">
                                <span class="glyphicon glyphicon-log-out" aria-hidden="true"></span>
                                <span>Keluar</span>
                            </a>
                        </li>
                        <!-- Control Sidebar Toggle Button -->

                    </ul>
                </div>
            </nav>
        </header>

        <!-- =============================================== -->

        <!-- Left side column. contains the sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar" >
                <!-- Sidebar user panel -->
                <div class="user-panel">
                    <div class="pull-left image">
                        <img class="img-circle" alt="User Image" src="template/dist/img/avatar5.png">
                    </div>
                    <div class="pull-left info">
                        <p><?php echo ucwords($_SESSION["admin"]);?></p>
                        <small><i class="fa fa-circle text-green"></i> Admin </small>
                    </div>
                </div>

                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu">

                    <li>
                        <a class="link" id="dh" href="./">
                            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                        </a>
                    </li>
                    <li class="header">Menu</li>
                    <li><a class="link" id="dk" href="./"><i class="fa fa-circle-o text-red"></i> <span>Data Karyawan</span></a></li>
                    <li><a class="link" id="lp" href="./"><i class="fa fa-circle-o text-yellow"></i> <span>Laporan Presensi</span></a></li>
                    <li><a class="link" id="if" href="./"><i class="fa fa-circle-o text-aqua"></i> <span>Informasi</span></a></li>
                    <li>
                        <a class="link" id="ht" href="./">
                            <i class="fa fa-info"></i> <span>How to use</span>
                        </a>
                    </li>
                    
                </ul>
            </section>
            <!-- /.sidebar -->
        </aside>

        <!-- =============================================== -->

        <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" style="min-height: 640px;">
            <!-- Content Header (Page header) -->
        <section class="content-header">
                <h1 id="judul"></h1>
        </section>

            <!-- Main content -->
        <section class="content">
                <!-- Default box -->
                <div id="main-content" class="row"> </div><!-- /.main-content -->
        </section><!-- /.content -->
    
    </div><!-- ./wrapper -->
    
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 1.1.0
        </div>
        <strong>Copyright © 2015-2016 <a href="./">PWH Lab</a>.</strong> All rights reserved.
    </footer>
    <!-- jQuery 2.1.4 -->
    <script src="template/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- action on klik-->
    <script type="text/javascript">
        $(document).ready(function(event){

            $('.link').click(function(event){
                event.preventDefault();
                var url_param=$(this).attr('id');
                load_page(url_param);
            });
            //load konten pada pertama kali
            var url_param=getURLParameter('page');
            load_page(url_param);

        });
        //memuat halaman
        var loading='<div id="loading" class="box box-widget"> <div class="overlay"><i class="fa fa-refresh fa-spin"></i></div><div class="box-header"></div><div class="box-body" style="height:300px;"></div><div class="box-footer"></div></div>';
        function load_page (page) {
            switch (page) { 
                            case 'dk': 
                                isi('Data Karyawan','datakaryawan.php');
                                break;
                            case 'lp': 
                                isi('Laporan Presensi','lappresensi.php');
                                break;
                            case 'if':
                                isi('Kalender Aktivitas <small>(Izin, Sakit, Cuti atau Absen)</small>','informasi.php');
                                break;
                            case 'ht':
                                isi('Cara Penggunaan <b>GeP</b>resence','howto.php #ad');
                                break;        
                            default:
                                isi('Dashboard Presensi','dashboard.php');
                        }
        }
        function isi(judul,url){
            $('#main-content').html(loading);
            $('#judul').html(judul+" ");  
            $('#main-content').load(url);
        }
        //mengambil parameter dalam URL
        function getURLParameter(sParam) {
            var sPageURL = window.location.search.substring(1);
            var sURLVariables = sPageURL.split('&');
            for (var i = 0; i < sURLVariables.length; i++)
            {
                var sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] == sParam)
                {
                    return sParameterName[1];
                }
            }
        }
    </script>
    <!-- jQuery-Confirm -->
    <script src="template/dist/js/jquery-confirm.min.js"></script>
    <!-- tombol keluar, pojok kanan atas-->
    <script type="text/javascript">
        $('#keluar').on('click', function () {
            $.confirm({
                theme: 'supervan',
                title: 'Konfirmasi',
                content: 'Anda yakin keluar?',
                confirmButton: 'Ya',cancelButton: 'Batal',
                confirm: function () {
                    window.location="action/cek-logout-admin.php";
                }
            });
        });
    </script>
    <!-- clock -->
    <script type="text/javascript">
    function updateClock ( )
    {
        var currentTime = new Date ( );
        var currentHours = currentTime.getHours ( );
        var currentMinutes = currentTime.getMinutes ( );
        var currentSeconds = currentTime.getSeconds ( );
        // Pad the minutes and seconds with leading zeros, if required
        currentMinutes = ( currentMinutes < 10 ? "0" : "" ) + currentMinutes;
        currentSeconds = ( currentSeconds < 10 ? "0" : "" ) + currentSeconds;
        // Compose the string for display
        var currentTimeString = currentHours + ":" + currentMinutes + ":" + currentSeconds;
        $("#clock").html(currentTimeString); }

        $(document).ready(function()
        {
           setInterval('updateClock()', 1000);
        });
    </script>
    <!-- fullCalendar 2.2.5 -->
    <script src="template/dist/js/moment.min.js"></script>
    <script src="template/plugins/fullcalendar/fullcalendar.min.js"></script>
    <!--qTip Tooltip for fullcalendar  -->
    <script src="template/dist/js/jquery.qtip.min.js"></script>
    <!--JQuery UI  -->
    <script src="template/dist/js/jquery-ui.min.js"></script>
    
    <script>
        // auto reload dashboard datang & pulang
        $(document).ready(function(){
        setInterval(function(){
        $(".datang-putar").load('datangdinamis.php')
        }, 5000);
        setInterval(function(){
        $(".pulang-putar").load('pulangdinamis.php')
        }, 5000); 
        });
    </script>
    <!-- DataTables -->
    <script src="template/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="template/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- datepicker js -->
    <script src="template/dist/js/bootstrap-datepicker.js"></script>
    <!-- bootstrap time picker -->
    <script src="template/plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <!-- Select2 -->
    <script src="template/plugins/select2/select2.full.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="template/bootstrap/js/bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="template/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="template/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="template/dist/js/app.min.js"></script>
    <!-- menambahkan bahasa -->
    <script src="template/dist/js/lang-all.js"></script>

    </body>

</html>
<?php 
} else { header("location:index.php"); }
?>
